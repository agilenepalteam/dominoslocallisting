﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;

namespace AppleAPITest.Helper
{
    public static class CommonHelper
    {
	    public static string FormatTitle(this string text)
	    {
		    var result = new StringBuilder();
		    if (!String.IsNullOrWhiteSpace(text))
		    {
			    int charLocation = text.IndexOf("(", StringComparison.Ordinal);

			    if (charLocation > 0)
			    {
				    string beforeCharString = text.Substring(0, charLocation);
				    var firstCaseUpperr = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(beforeCharString.ToLower());
				    result.Append("Domino’s Pizza ");
				    result.Append(firstCaseUpperr);
				    result.Append(text.Substring(text.LastIndexOf('(')));
				    return result.ToString();
			    }
			    else
			    {
				    result.Append("Domino’s Pizza ");
				    result.Append(CultureInfo.CurrentCulture.TextInfo.ToTitleCase(text.ToLower()));
				    return result.ToString();
			    }
		    }
		    return String.Empty;
	    }

		public static void UpdateLocationLog(List<DataModel.LocationLogDetails> locationDetails, string locationCode,
            string type, string updateStatus, string message)
        {
            var location = locationDetails.FirstOrDefault(x => x.LocationCode == locationCode);
            if (location != null)
            {
                switch (type)
                {
                    case "apple":
                        location.AppleStatus = updateStatus;
                        location.AppleMessage = message;
                        break;
                    case "location":
                        location.LocationStatus = updateStatus;
                        location.LocationMessage = message;
                        break;
                    default:
                        location.GMBStatus = updateStatus;
                        location.GMBMessage = message;
                        break;
                }
            }
        }

        public static string GetDayText(string day)
        {
            switch (day)
            {
                case "0":
                    return "Su";
                case "1":
                    return "Mo";
                case "2":
                    return "Tu";
                case "3":
                    return "We";
                case "4":
                    return "Th";
                case "5":
                    return "Fr";
                case "6":
                    return "Sa";
            }

            return "";
        }

        public static int GetDayInt(string day)
        {
            switch (day.ToUpper())
            {
                case "SUNDAY":
                    return 0;
                case "MONDAY":
                    return 1;
                case "TUESDAY":
                    return 2;
                case "WEDNESDAY":
                    return 3;
                case "THURSDAY":
                    return 4;
                case "FRIDAY":
                    return 5;
                case "SATURDAY":
                    return 6;
            }

            return 0;
        }

        public static void SendSummaryEmail(List<DataModel.LocationLogDetails> locationDetails)
        {
            var emailBody = "";
            var emailSubject = "Summary Email";
            emailBody = EmailTemplate.GetMailBody("DetailBody", "","", locationDetails);
            EmailTemplate.SendEmail(emailSubject, emailBody );
        }
    }
}