﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;

namespace AppleAPITest.Helper
{
    public static class AppleHelper
    {
		public static async System.Threading.Tasks.Task<string> GetAppleAccessTokenAsync()
		{
			//			var jsonData = @"{
			//  ""client_id"": ""148b9a0c-ae43-6400-15d4-591fbde49400"",
			//  ""client_secret"": ""2JzC9N6tb8oQrW7qnd4RIvvRJ49VmgR1KOjKIRxSQLeHZtzxsg8B4bBtLR9S8u8d"",
			//  ""grant_type"": ""client_credentials"",
			//  ""scope"": ""business_connect""
			//}";

			var jsonData = @"{
  ""client_id"": ""148b9a0c-ae43-6400-1542-1bdfcab0b800"",
  ""client_secret"": ""VMsqKS12lckg7YcpTw5YfIRVeV4fLkQT6CcLZbArWL5cd9J4Ic3ijXw7WdleH4g8"",
  ""grant_type"": ""client_credentials"",
  ""scope"": ""business_connect""
}";
			var appleURL = "https://data-qualification.businessconnect.apple.com/api/v1/oauth2/token";
			//var appleURL = "https://api-qualification.businessconnect.apple.com/api/v1/oauth2/token";

			try
			{
				using (HttpClient client = new HttpClient())
				{
					// Set the content type to JSON
					client.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));

					// Create the HTTP request message with the JSON data
					HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Post, appleURL);
					request.Content = new StringContent(jsonData, Encoding.UTF8, "application/json");

					// Send the POST request
					HttpResponseMessage response = await client.SendAsync(request);

					// Check if the request was successful (status code 200 OK)
					if (response.IsSuccessStatusCode)
					{
						// Read and print the JSON response
						string jsonResponse = await response.Content.ReadAsStringAsync();
						Console.WriteLine("JSON Response:");
						Console.WriteLine(jsonResponse);

						JObject json = JObject.Parse(jsonResponse);

						// Extract the access_token
						string accessToken = json["access_token"]?.ToString();
						return accessToken;
						if (!string.IsNullOrEmpty(accessToken))
						{
							Console.WriteLine("Access Token: " + accessToken);
						}
						else
						{
							Console.WriteLine("Access Token not found in the JSON response.");
						}
					}
					else
					{
						Console.WriteLine($"Error: {response.StatusCode} - {response.ReasonPhrase}");
					}
				}

			}
			catch (Exception ex)
			{
				Console.WriteLine($"Exception: {ex.Message}");
			}

			return "";
		}

		public static string GetModifiedURl(string url)
		{
			var urlToBeModified = new Uri(url);
			var urlWithOutQuery = urlToBeModified.GetLeftPart(UriPartial.Path);
			var urlQuery = urlToBeModified.Query;
			var urlQueryKeyValue = System.Web.HttpUtility.ParseQueryString(urlQuery);



			// Remove all query keys that starts with utm
			foreach (var key in urlQueryKeyValue.AllKeys)
			{
				if (key.ToLower().StartsWith("utm"))
					urlQueryKeyValue.Remove(key);
			}



			// Now we only have keys that does not start with utm and using these keys lets create the URL again
			if (urlQueryKeyValue.Count > 0)
			{
				var newQuery = string.Join("&", urlQueryKeyValue.AllKeys.Select(key => key.ToString() + "=" + urlQueryKeyValue[key.ToString()]));
				return urlWithOutQuery + "?" + newQuery;
			}



			return urlWithOutQuery;



		}
		public static string GetFormattedRoutine(List<DataModel.OpeningHoursDTO> openingHourDto)
		{
			#region Variable declaration
			//Default Day order
			var dayArray = new List<string>() { "Su", "Mo", "Tu", "We", "Th", "Fr", "Sa" };
			// final routine string that will be returned by this method
			var finalDayString = "";
			var firstDay = "";
			var lastDay = "";
			var currentOpenTime = "";
			var currentCloseTime = "";

			foreach (var o in openingHourDto)
			{
				o.Day = CommonHelper.GetDayText(o.Day);
			}



			#endregion
			//var openingHourDto = new List<DataModel.OpeningHoursDTO>();
			//var routineArray = routineString.Split(';');
			//foreach (var item in routineArray)
			//{
			//	if (!string.IsNullOrWhiteSpace(item))
			//	{
			//		var day = item.Split(' ').First();
			//		var openTime = item.Split(' ').Last().Split('-').First();
			//		var closeTime = item.Split(' ').Last().Split('-').Last();
			//		var model = new DataModel.OpeningHoursDTO()
			//		{
			//			Day = day,
			//			OpenTime = openTime,
			//			CloseTime = closeTime
			//		};
			//		openingHourDto.Add(model);
			//	}
			//}
			foreach (var day in dayArray)
			{
				var dayDto = openingHourDto.Where(x => x.Day.ToLower() == day.ToLower()).FirstOrDefault();
				if (dayDto != null)
				{
					//there is no previous value so insert the current one
					if (string.IsNullOrWhiteSpace(firstDay) && string.IsNullOrWhiteSpace(currentOpenTime) && string.IsNullOrWhiteSpace(currentCloseTime))
					{
						firstDay = day;
						currentOpenTime = dayDto.OpenTime;
						currentCloseTime = dayDto.CloseTime;
					}
					//there is already previous value so campare this value with previous one using OpenHour and closeHour
					else
					{
						var isEqual = string.Equals(currentOpenTime.Trim(), dayDto.OpenTime.Trim()) && string.Equals(currentCloseTime.Trim(), dayDto.CloseTime.Trim());
						if (!isEqual)
						{
							// since these two data have different time, add the previous one to finalDayString string
							if (!string.IsNullOrWhiteSpace(firstDay) && !string.IsNullOrWhiteSpace(lastDay))
								finalDayString = finalDayString + firstDay + "-" + lastDay + " " + currentOpenTime + "-" + currentCloseTime + ";";
							if (!string.IsNullOrWhiteSpace(firstDay) && string.IsNullOrWhiteSpace(lastDay))
								finalDayString = finalDayString + firstDay + " " + currentOpenTime + "-" + currentCloseTime + ";";



							// Now replace value with current data
							firstDay = day;
							lastDay = "";
							currentOpenTime = dayDto.OpenTime;
							currentCloseTime = dayDto.CloseTime;
						}
						//since Open and Close time are same only update value for lastDay
						else
						{
							lastDay = day;
						}



					}
				}
				else
				{
					if (!string.IsNullOrWhiteSpace(firstDay) && !string.IsNullOrWhiteSpace(lastDay))
						finalDayString = finalDayString + firstDay + "-" + lastDay + " " + currentOpenTime + "-" + currentCloseTime + ";";
					if (!string.IsNullOrWhiteSpace(firstDay) && string.IsNullOrWhiteSpace(lastDay))
						finalDayString = finalDayString + firstDay + " " + currentOpenTime + "-" + currentCloseTime + ";";
					//Reset value
					firstDay = "";
					lastDay = "";
					currentOpenTime = "";
					currentCloseTime = "";



				}
			}
			if (!string.IsNullOrWhiteSpace(firstDay) && !string.IsNullOrWhiteSpace(lastDay))
				finalDayString = finalDayString + firstDay + "-" + lastDay + " " + currentOpenTime + "-" + currentCloseTime + ";";
			if (!string.IsNullOrWhiteSpace(firstDay) && string.IsNullOrWhiteSpace(lastDay))
				finalDayString = finalDayString + firstDay + " " + currentOpenTime + "-" + currentCloseTime + ";";
			return finalDayString;
		}
		public static string GetAppleJson(List<DataModel.BusinessListingDTO> businessListing, List<DataModel.LocationLogDetails> locationDetails)
        {
            var appleJsonString = "";

            businessListing = businessListing.OrderBy(x => x.LocationName.FirstOrDefault().Name).ToList();

			foreach (var listing in businessListing)
            {
                var appleJsonLine = "";
                try
                {
                    if (!string.IsNullOrEmpty(listing.LocationCode))
                    {
	                    appleJsonLine = appleJsonLine + "\"site_code\": \"" + listing.Chain + "_" +
	                                    listing.LocationCode + "\"";


                        if (listing.LocationName.Any())
                        {
                            var nameValue = "";
                            foreach (var name in listing.LocationName)
                            {
                                if (nameValue != "")
                                {
                                    nameValue = nameValue + ",";
                                }

								nameValue = nameValue + "{ \"name\": \"" + name.Name + "\", \"locale\": \"" +
                                            name.Locale +
                                            "\" }";
                            }

                            appleJsonLine = appleJsonLine + ",\"names\": [" + nameValue + "  ]";
                        }

                        if (listing.AlternativeLocationName.Any())
                        {
                            var nameValue = "";
                            foreach (var name in listing.AlternativeLocationName)
                            {
                                if (nameValue != "")
                                {
                                    nameValue = nameValue + ",";
                                }

                                nameValue = nameValue + "{ \"name\": \"" + name.Name + "\", \"locale\": \"" +
                                            name.Locale +
                                            "\" }";
                            }

                            appleJsonLine = appleJsonLine + ",\"alternate_names\": [" + nameValue + "  ]";
                        }

                        if (listing.BusinessStatus.Any())
                        {
                            var bs = listing.BusinessStatus.FirstOrDefault();
                            if (bs.Status == "OPEN")
                            {
                                appleJsonLine = appleJsonLine + ",\"business_status\": { \"status\": \"OPEN\" } ";
                            }
                            else if (bs.Status == "CLOSED_PERMANENTLY")
                            {
                                var d = ((DateTime) bs.CloseDate).ToString("yyyy-MM-dd");
                                appleJsonLine = appleJsonLine +
												",\"business_status\": { \"status\": \"CLOSED\", \"closed_date\": \"" +
                                                d + "\" } ";
                            }
                            else if (bs.Status == "CLOSED_TEMPORARILY")
                            {
                                var d = ((DateTime) bs.ReOpenDate).ToString("yyyy-MM-dd");
                                appleJsonLine = appleJsonLine +
												",\"business_status\": { \"status\": \"TEMPORARILY_CLOSED\", \"reopen_date\": \"" +
                                                d + "\" } ";
                            }

                        }
                        else
                        {
                            appleJsonLine = appleJsonLine + ",\"business_status\": { \"status\": \"Open\" } ";
                        }

                        if (listing.PostalAddress != null)
                        {
                            if (listing.PostalAddress.StructuredAddress.Any())
                            {
                                var address = listing.PostalAddress.StructuredAddress.FirstOrDefault();
                                if (address != null)
                                {
                                    if (address.AddressLines.Any() || address.CountryCode != "" ||
                                        address.Floor != "" ||
                                        address.Locality != "" || address.Neighborhood != "" ||
                                        address.PostalCode != "" ||
                                        address.StateProvince != "")
                                    {
                                        appleJsonLine =
                                            appleJsonLine + ",\"main_address\": { \"structured_address\": { ";
                                        appleJsonLine = appleJsonLine + "\"street_address\": [ \"" +
                                                        address.AddressLines.FirstOrDefault() + "\" ]";
                                        appleJsonLine = appleJsonLine + ",\"locality\": \"" + address.Locality + "\"";
                                        appleJsonLine =
                                            appleJsonLine + ",\"state_province\": \"" + address.StateProvince +
                                            "\"";
                                        appleJsonLine = appleJsonLine + ",\"country_code\": \"" + address.CountryCode +
                                                        "\"";
                                        appleJsonLine =
                                            appleJsonLine + ",\"postal_code\": \"" + address.PostalCode + "\"";
                                        appleJsonLine = appleJsonLine + "}}";
                                    }
                                }
                            }
                        }

                        if (listing.DisplayPoint != null)
                        {
	                        if (listing.DisplayPoint.Coordinates.Longitude != null &&
	                            listing.DisplayPoint.Coordinates.Latitude != null)
	                        {
		                        appleJsonLine = appleJsonLine +
		                                        ",\"display_point\": {\"coordinates\": { \"latitude\": \"" +
		                                        listing.DisplayPoint.Coordinates.Longitude + "\", \"longitude\": \"" +
		                                        listing.DisplayPoint.Coordinates.Latitude + "\" },\"source\": \"" +
		                                        listing.DisplayPoint.Source + "\"}";
	                        }
                        }

                        if (listing.HomePage != null)
                        {
                            appleJsonLine = appleJsonLine + ",\"home_page\": \"" + GetModifiedURl(listing.HomePage) + "\"";
                        }

                        if (listing.PhoneNumber != null)
                        {
	                        var formattedNumber = "";
	                        if (listing.Locale.ToLower() == "en-au")
	                        {
		                        formattedNumber = "+61" + listing.PhoneNumber.PhoneNumber;
	                        }
	                        else if (listing.Locale.ToLower() == "en-nz")
	                        {
		                        formattedNumber = "+64" + listing.PhoneNumber.PhoneNumber;
	                        }

							appleJsonLine = appleJsonLine + ",\"phone_number\": { \"number\": \"" +
							                formattedNumber + "\", \"type\": \"" +
                                            listing.PhoneNumber.Type + "\" } ";
                        }

                        if (listing.Categories.Any())
                        {
                            var categoryString = "";
                            foreach (var category in listing.Categories)
                            {
                                if (categoryString != "")
                                {
                                    categoryString = categoryString + ",";
                                }

                                categoryString = categoryString + "\"" + category.Category + "\"";
                            }

                            appleJsonLine = appleJsonLine + ",\"categories\": [" + categoryString + "]";
                        }

                        if (listing.OpeningHours.Any())
                        {
                            var hourString = "";
							//foreach (var oh in listing.OpeningHours)
							//{
							//    if (oh.Day != "")
							//    {
							//        hourString = hourString + CommonHelper.GetDayText(oh.Day) + " " + oh.OpenTime + "-" +
							//                     oh.CloseTime + ";";
							//    }
							//}
							hourString = GetFormattedRoutine(listing.OpeningHours);

							appleJsonLine = appleJsonLine + ",\"hours\": \"" + hourString + "\"";
                        }

                        //if (listing.Chain.Any())
                        //{
                        //    //var chainString = "";
                        //    //foreach (var chain in listing.Chain)
                        //    //{
                        //    //    if (chainString != "")
                        //    //    {
                        //    //        chainString = chainString + ",";
                        //    //    }

                        //    //    chainString = chainString + "\"" + chain + "\"";
                        //    //}

                        //    appleJsonLine = appleJsonLine + ",\"brand\": {\"id\": \"dominos-brand-001\", \"name\": " + listing.Chain + "}";
                        //}

                        if (listing.Content != null)
                        {

	                        if (listing.Content.Any())
	                        {
		                        var nameValue = "";
		                        foreach (var name in listing.Content)
		                        {
			                        if (nameValue != "")
			                        {
				                        nameValue = nameValue + ",";
			                        }

			                        if (name.Content != "")
			                        {
				                        nameValue = nameValue + "{ \"short_abstract\": \"" + name.Content +
				                                    "\", \"locale\": \"" +
				                                    name.Locale +
				                                    "\" }";
			                        }
		                        }

		                        if (nameValue != "")
		                        {
			                        appleJsonLine = appleJsonLine + ",\"content\": [" + nameValue + "  ]";
		                        }
	                        }
                        }

                        appleJsonLine = "{" + appleJsonLine + "}";

                        appleJsonString = appleJsonString + appleJsonLine + Environment.NewLine;

                        CommonHelper.UpdateLocationLog(locationDetails, listing.LocationCode, "apple", "JSON-CREATED", "");
                    }
                }
                catch (Exception ex)
                {
                    CommonHelper.UpdateLocationLog(locationDetails, listing.LocationCode, "apple", "JSON-ERROR", ex.Message);

                    Logger.FileLogger(ex.Message, Parameters.LogFile.LogFile.ToString());
                    Logger.FileLogger(ex, Parameters.LogFile.ErrorLogFile.ToString());
                }
            }
            
            return appleJsonString;
        }
		public static string GetAppleLocationObject(List<DataModel.BusinessListingDTO> businessListing, List<DataModel.LocationLogDetails> locationDetails)
		{
			var appleJsonString = "";

			businessListing = businessListing.OrderBy(x => x.LocationName.FirstOrDefault().Name).ToList();

			foreach (var listing in businessListing)
			{
				var appleJsonLine = "";
				try
				{
					if (!string.IsNullOrEmpty(listing.LocationCode))
					{
						appleJsonLine = appleJsonLine + "\"site_code\": \"" + listing.Chain + "_" +
										listing.LocationCode + "\"";


						if (listing.LocationName.Any())
						{
							var nameValue = "";
							foreach (var name in listing.LocationName)
							{
								if (nameValue != "")
								{
									nameValue = nameValue + ",";
								}

								nameValue = nameValue + "{ \"name\": \"" + name.Name + "\", \"locale\": \"" +
											name.Locale +
											"\" }";
							}

							appleJsonLine = appleJsonLine + ",\"names\": [" + nameValue + "  ]";
						}

						if (listing.AlternativeLocationName.Any())
						{
							var nameValue = "";
							foreach (var name in listing.AlternativeLocationName)
							{
								if (nameValue != "")
								{
									nameValue = nameValue + ",";
								}

								nameValue = nameValue + "{ \"name\": \"" + name.Name + "\", \"locale\": \"" +
											name.Locale +
											"\" }";
							}

							appleJsonLine = appleJsonLine + ",\"alternate_names\": [" + nameValue + "  ]";
						}

						if (listing.BusinessStatus.Any())
						{
							var bs = listing.BusinessStatus.FirstOrDefault();
							if (bs.Status == "OPEN")
							{
								appleJsonLine = appleJsonLine + ",\"business_status\": { \"status\": \"OPEN\" } ";
							}
							else if (bs.Status == "CLOSED_PERMANENTLY")
							{
								var d = ((DateTime)bs.CloseDate).ToString("yyyy-MM-dd");
								appleJsonLine = appleJsonLine +
												",\"business_status\": { \"status\": \"CLOSED\", \"closed_date\": \"" +
												d + "\" } ";
							}
							else if (bs.Status == "CLOSED_TEMPORARILY")
							{
								var d = ((DateTime)bs.ReOpenDate).ToString("yyyy-MM-dd");
								appleJsonLine = appleJsonLine +
												",\"business_status\": { \"status\": \"TEMPORARILY_CLOSED\", \"reopen_date\": \"" +
												d + "\" } ";
							}

						}
						else
						{
							appleJsonLine = appleJsonLine + ",\"business_status\": { \"status\": \"Open\" } ";
						}

						if (listing.PostalAddress != null)
						{
							if (listing.PostalAddress.StructuredAddress.Any())
							{
								var address = listing.PostalAddress.StructuredAddress.FirstOrDefault();
								if (address != null)
								{
									if (address.AddressLines.Any() || address.CountryCode != "" ||
										address.Floor != "" ||
										address.Locality != "" || address.Neighborhood != "" ||
										address.PostalCode != "" ||
										address.StateProvince != "")
									{
										appleJsonLine =
											appleJsonLine + ",\"main_address\": { \"structured_address\": { ";
										appleJsonLine = appleJsonLine + "\"street_address\": [ \"" +
														address.AddressLines.FirstOrDefault() + "\" ]";
										appleJsonLine = appleJsonLine + ",\"locality\": \"" + address.Locality + "\"";
										appleJsonLine =
											appleJsonLine + ",\"state_province\": \"" + address.StateProvince +
											"\"";
										appleJsonLine = appleJsonLine + ",\"country_code\": \"" + address.CountryCode +
														"\"";
										appleJsonLine =
											appleJsonLine + ",\"postal_code\": \"" + address.PostalCode + "\"";
										appleJsonLine = appleJsonLine + "}}";
									}
								}
							}
						}

						if (listing.DisplayPoint != null)
						{
							if (listing.DisplayPoint.Coordinates.Longitude != null &&
								listing.DisplayPoint.Coordinates.Latitude != null)
							{
								appleJsonLine = appleJsonLine +
												",\"display_point\": {\"coordinates\": { \"latitude\": \"" +
												listing.DisplayPoint.Coordinates.Longitude + "\", \"longitude\": \"" +
												listing.DisplayPoint.Coordinates.Latitude + "\" },\"source\": \"" +
												listing.DisplayPoint.Source + "\"}";
							}
						}

						if (listing.HomePage != null)
						{
							appleJsonLine = appleJsonLine + ",\"home_page\": \"" + GetModifiedURl(listing.HomePage) + "\"";
						}

						if (listing.PhoneNumber != null)
						{
							var formattedNumber = "";
							if (listing.Locale.ToLower() == "en-au")
							{
								formattedNumber = "+61" + listing.PhoneNumber.PhoneNumber;
							}
							else if (listing.Locale.ToLower() == "en-nz")
							{
								formattedNumber = "+64" + listing.PhoneNumber.PhoneNumber;
							}

							appleJsonLine = appleJsonLine + ",\"phone_number\": { \"number\": \"" +
											formattedNumber + "\", \"type\": \"" +
											listing.PhoneNumber.Type + "\" } ";
						}

						if (listing.Categories.Any())
						{
							var categoryString = "";
							foreach (var category in listing.Categories)
							{
								if (categoryString != "")
								{
									categoryString = categoryString + ",";
								}

								categoryString = categoryString + "\"" + category.Category + "\"";
							}

							appleJsonLine = appleJsonLine + ",\"categories\": [" + categoryString + "]";
						}

						if (listing.OpeningHours.Any())
						{
							var hourString = "";
							//foreach (var oh in listing.OpeningHours)
							//{
							//    if (oh.Day != "")
							//    {
							//        hourString = hourString + CommonHelper.GetDayText(oh.Day) + " " + oh.OpenTime + "-" +
							//                     oh.CloseTime + ";";
							//    }
							//}
							hourString = GetFormattedRoutine(listing.OpeningHours);

							appleJsonLine = appleJsonLine + ",\"hours\": \"" + hourString + "\"";
						}

						//if (listing.Chain.Any())
						//{
						//    //var chainString = "";
						//    //foreach (var chain in listing.Chain)
						//    //{
						//    //    if (chainString != "")
						//    //    {
						//    //        chainString = chainString + ",";
						//    //    }

						//    //    chainString = chainString + "\"" + chain + "\"";
						//    //}

						//    appleJsonLine = appleJsonLine + ",\"brand\": {\"id\": \"dominos-brand-001\", \"name\": " + listing.Chain + "}";
						//}

						if (listing.Content != null)
						{

							if (listing.Content.Any())
							{
								var nameValue = "";
								foreach (var name in listing.Content)
								{
									if (nameValue != "")
									{
										nameValue = nameValue + ",";
									}

									if (name.Content != "")
									{
										nameValue = nameValue + "{ \"short_abstract\": \"" + name.Content +
													"\", \"locale\": \"" +
													name.Locale +
													"\" }";
									}
								}

								if (nameValue != "")
								{
									appleJsonLine = appleJsonLine + ",\"content\": [" + nameValue + "  ]";
								}
							}
						}

						appleJsonLine = "{" + appleJsonLine + "}";

						appleJsonString = appleJsonString + appleJsonLine + Environment.NewLine;

						CommonHelper.UpdateLocationLog(locationDetails, listing.LocationCode, "apple", "JSON-CREATED", "");
					}
				}
				catch (Exception ex)
				{
					CommonHelper.UpdateLocationLog(locationDetails, listing.LocationCode, "apple", "JSON-ERROR", ex.Message);

					Logger.FileLogger(ex.Message, Parameters.LogFile.LogFile.ToString());
					Logger.FileLogger(ex, Parameters.LogFile.ErrorLogFile.ToString());
				}
			}

			return appleJsonString;
		}



	}
}