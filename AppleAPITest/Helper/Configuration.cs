﻿using System.Configuration;

namespace AppleAPITest.Helper
{
    public static class Configuration
    {
        public static string DominosUrl => ConfigurationManager.AppSettings["DominosUrl"];

        public static string CsvFileLocation => ConfigurationManager.AppSettings["CsvFileLocation"];

        public static string PgConnectionString => ConfigurationManager.AppSettings["PgConnectionString"];

        public static string SmtpClient => ConfigurationManager.AppSettings["SmtpClient"];
        public static string SmtpNetworkCredentialUsername => ConfigurationManager.AppSettings["SmtpNetworkCredentialUsername"];
        public static string SmtpNetworkCredentialPassword => ConfigurationManager.AppSettings["SmtpNetworkCredentialPassword"];
        public static string SmtpPort => ConfigurationManager.AppSettings["SmtpPort"];
        public static string SmtpDisplayNameForFromAddress => ConfigurationManager.AppSettings["SmtpDisplayNameForFromAddress"];
        public static string SmtpFromAddress => ConfigurationManager.AppSettings["SmtpFromAddress"];
        public static string SmtpReplyAddress => ConfigurationManager.AppSettings["SmtpReplyAddress"];
        public static string SmtpDisplayNameForReplyAddress => ConfigurationManager.AppSettings["SmtpDisplayNameForReplyAddress"];
        public static string SmtpEnableSsl => ConfigurationManager.AppSettings["SmtpEnableSSL"];

        public static string Recipient => ConfigurationManager.AppSettings["Recipient"];
    }
}
