﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using CsvHelper;
using CsvHelper.Configuration;
using Dapper;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Npgsql;

namespace AppleAPITest.Helper
{
    public static class BusinessListingHelper
    {
        public static List<DataModel.BusinessListingDTO> GetDominosBusinessListing(
            List<DataModel.LocationLogDetails> locationDetails, string dataSource)
        {
            Logger.FileLogger("****** GETTING JSON FROM URL *******", Parameters.LogFile.LogFile.ToString());

            var jsonUrl = Configuration.DominosUrl;
            var jsonString = "";
            var jsonErrorMessage = "";

            try
            {
                using (var wc = new WebClient())
                {
                    jsonString = wc.DownloadString(jsonUrl);
                }
            }
            catch (Exception ex)
            {
                jsonErrorMessage = ex.Message;
            }

            if (jsonString.Trim() == "")
            {
                Logger.FileLogger("****** GETTING JSON FROM URL - EMPTY JSON *******",
                    Parameters.LogFile.LogFile.ToString());

                var emailSubject = "Empty JSON";
                var emailBody = EmailTemplate.GetMailBody("PlainBody", EmailTemplate.EmptyResponse, jsonErrorMessage);

                EmailTemplate.SendEmail(emailSubject,emailBody );

                return new List<DataModel.BusinessListingDTO>();
            }

            Logger.FileLogger("****** GETTING JSON FROM URL COMPLETED *******", Parameters.LogFile.LogFile.ToString());

            dynamic jsonObject;
            try
            {
                jsonObject = JsonConvert.DeserializeObject<dynamic>(jsonString);
            }
            catch (Exception ex)
            {
                Logger.FileLogger("****** GETTING JSON FROM URL - INVALID JSON *******",
                    Parameters.LogFile.LogFile.ToString());

                var emailSubject = "Invalid Json";
                var emailBody = EmailTemplate.GetMailBody("PlainBody", EmailTemplate.InvalidJson);
                EmailTemplate.SendEmail(emailBody, emailSubject);

                Logger.FileLogger(ex.Message, Parameters.LogFile.LogFile.ToString());
                Logger.FileLogger(ex, Parameters.LogFile.ErrorLogFile.ToString());

                return new List<DataModel.BusinessListingDTO>();
            }

            Logger.FileLogger("****** GETTING JSON FROM URL - PARSING JSON *******",
                Parameters.LogFile.LogFile.ToString());

            var listingObject = new List<DataModel.BusinessListingDTO>();

            foreach (var jsonLine in jsonObject)
            {
                try
                {
                    var locationCode = jsonLine["StoreNo"].Value.ToString().Trim();
                    var locationName = jsonLine["Name"].Value.ToString().Trim();

                    locationDetails.Add(new DataModel.LocationLogDetails()
                        {LocationCode = locationCode, LocationName = locationName});

                    var locationAlternativeName = jsonLine["FullName"].Value.ToString().Trim();
                    //var locationBusinessStatus = jsonLine["Closures"];
                    var locationAddressStreetName = jsonLine["Address"]["StreetName"].Value.ToString().ToString()
                        .Replace("\r", " ").Replace("\n", " ").Replace("'", "").Trim();
                    var locationAddressStreetSuburb = jsonLine["Address"]["Suburb"].Value.ToString().Replace("\r", " ")
                        .Replace("\n", " ").Replace("'", "").Trim();
                    var locationAddressStreetState = jsonLine["Address"]["State"].Value.ToString().Replace("\r", " ")
                        .Replace("\n", " ").Replace("'", "").Trim();
                    var locationAddressStreetPostalCode = jsonLine["Address"]["PostalCode"].Value.ToString()
                        .Replace("\r", " ").Replace("'", "").Replace("\n", " ").Trim();
                    var locationGeoCoordinatesLongitude =
                        jsonLine["GeoCoordinates"]["Longitude"].Value.ToString().Trim();

                    var locale = locationAddressStreetState == "NZ" ? "en-NZ" : "en-AU";
                   // var country = locationAddressStreetState == "NZ" ? "NZ" : "AU";

					var locationGeoCoordinatesLatitude = jsonLine["GeoCoordinates"]["Latitude"].Value.ToString().Trim();
                    var locationUrl = jsonLine["StoreDirectUrl"].Value.ToString().Trim();
                    string locationPhoneNo = jsonLine["PhoneNo"].Value.ToString().Trim();
                    if (locationPhoneNo.StartsWith("+"))
                    {
	                    locationPhoneNo = locationPhoneNo.ToString().Substring(1, locationPhoneNo.Length - 1);
                    }

					if (locationPhoneNo.StartsWith("0"))
					{
						locationPhoneNo = locationPhoneNo.ToString().Substring(1, locationPhoneNo.Length - 1);
					}

					if (locationPhoneNo.StartsWith("64") && locationPhoneNo.Length > 8)
					{
						locationPhoneNo = locationPhoneNo.ToString().Substring(2, locationPhoneNo.Length - 2);
					}

					if (locationPhoneNo.StartsWith("61") && locationPhoneNo.Length > 8)
					{
						locationPhoneNo = locationPhoneNo.ToString().Substring(2, locationPhoneNo.Length - 2);
					}

					locationPhoneNo = locationPhoneNo.Replace(" ", "").Replace("-", "");

					var content = jsonLine["Description"].Value.ToString().Trim();
					//telephone country code Australia			64
					//telephone country code new zealand		61

					//var locationDescription = jsonLine["Description"].Value.ToString().Trim();
					var locationOpeningHours = new List<DataModel.OpeningHoursDTO>();

                    foreach (var o in jsonLine["OpeningHours"])
                    {
                        var open = o["Open"].Value;
                        var close = o["Close"].Value;

                        DateTime openDate = Convert.ToDateTime(open);
                        DateTime closeDate = Convert.ToDateTime(close);

                        var day = ((int) openDate.DayOfWeek).ToString();
                        var openTime = openDate.ToString("HH:mm");
                        var closeTime = closeDate.ToString("HH:mm");

                        locationOpeningHours.Add(new DataModel.OpeningHoursDTO()
                            {Day = day, OpenTime = openTime, CloseTime = closeTime});
                    }

                    Logger.FileLogger("****** " + locationCode + " / " + locationName,
                        Parameters.LogFile.LogFile.ToString());

                    

					//CREATE LISTING MODEL
					var listing = new DataModel.BusinessListingDTO
                        {DataSource = dataSource, LocationCode = locationCode};

					//need to add this line
					listing.Locale = locale;

                    var ln = new DataModel.StoreNameDTO {Name = locationName, Locale = locale, Pronunciation = ""};
                    listing.LocationName = new List<DataModel.StoreNameDTO>() {ln};

                    var aln = new DataModel.StoreNameDTO
                        {Name = locationAlternativeName, Locale = locale, Pronunciation = ""};
                    listing.AlternativeLocationName = new List<DataModel.StoreNameDTO>() {aln};

                    var bs = new List<DataModel.BusinessStatusDTO>();
                    listing.BusinessStatus = bs;

                    var address = new DataModel.PostalAddressDTO {FullAddress = ""};
                    var sa = new DataModel.StructuredAddressDTO
                    {
                        AddressLines = new List<string>() {locationAddressStreetName},
                        Floor = "",
                        Neighborhood = "",
                        Locality = locationAddressStreetSuburb,
                        StateProvince = locationAddressStreetState,
                        PostalCode = locationAddressStreetPostalCode,
                        CountryCode = locationAddressStreetState == "NZ" ? "NZ" : "AU"
                    };
                    address.StructuredAddress = new List<DataModel.StructuredAddressDTO>() {sa};
                    listing.PostalAddress = address;

                    var ddp = new DataModel.DisplayPointDTO
                    {
                        Coordinates = new DataModel.CoordinatesDTO()
                        {
                            Latitude = locationGeoCoordinatesLongitude,
                            Longitude = locationGeoCoordinatesLatitude
                        },
                        Source = "MANUALLY_PLACED"
					};
                    listing.DisplayPoint = ddp;

                    listing.HomePage = locationUrl;

                    var poh = new DataModel.PhoneNumberDTO
                        {PhoneNumber = locationPhoneNo, Intent = "", Type = "Landline"};
                    listing.PhoneNumber = poh;


					var c = new DataModel.CategoryDTO() {Category = "restaurants.pizza"};
                    listing.Categories = new List<DataModel.CategoryDTO>() {c};

                    listing.OpeningHours = locationOpeningHours;

                    listing.Chain = dataSource;

                   
                    var contentValue = new DataModel.ContentDTO { Content = content, Locale = locale };
                    listing.Content = new List<DataModel.ContentDTO> { contentValue };

					listingObject.Add(listing);

                    CommonHelper.UpdateLocationLog(locationDetails, locationCode, "location", "PARSED", "");
                }
                catch (Exception ex)
                {
                    var storeCode = (jsonLine["StoreNo"]?.Value ?? "").ToString().Trim();

                    CommonHelper.UpdateLocationLog(locationDetails, storeCode, "location", "PARSE-ERROR", ex.Message);

                    Logger.FileLogger(ex.Message, Parameters.LogFile.LogFile.ToString());
                    Logger.FileLogger(ex, Parameters.LogFile.ErrorLogFile.ToString());
                }
            }

            return listingObject;
        }

        public static void OverrideBusinessListing(List<DataModel.BusinessListingDTO> businessListing,
            List<DataModel.LocationLogDetails> locationDetails)
        {
            List<DataModel.CSVFileDataDTO> csvData;
            var config = new CsvConfiguration(CultureInfo.InvariantCulture)
            {
                HasHeaderRecord = true,
            };
            using (var reader = new StreamReader(Configuration.CsvFileLocation))
            using (var csv = new CsvReader(reader, config))
            {
                csvData = csv.GetRecords<DataModel.CSVFileDataDTO>().ToList();
            }

            foreach (var businessObj in businessListing)
            {
                if (!string.IsNullOrEmpty(businessObj.LocationCode))
                {
                    var hasLocationCode = csvData.FirstOrDefault(x => x.LocationCode == businessObj.LocationCode);
                    if (hasLocationCode != null)
                    {
                        try
                        {
                            Logger.FileLogger(
                                $"****** Updating value for LocationCode {businessObj.LocationCode} *******",
                                Parameters.LogFile.LogFile.ToString());
                            var bk = businessObj.LocationName.FirstOrDefault();
                            if (bk != null)
                            {
                                bk.Name = hasLocationCode.LocationName;
                            }

                            CommonHelper.UpdateLocationLog(locationDetails, businessObj.LocationCode, "location",
                                "OVERRIDE", "");
                        }
                        catch (Exception ex)
                        {
                            Logger.FileLogger(
                                $"****** Error occured while updating for LocationCode {businessObj.LocationCode}. Error message is {ex.Message} *******",
                                Parameters.LogFile.ErrorLogFile.ToString());
                        }
                    }
                }
            }
        }

        public static void GetUpdatedBusiness(List<DataModel.BusinessListingDTO> businessListing,
            List<DataModel.LocationLogDetails> locationDetails)
        {
            var connection = new NpgsqlConnection(Configuration.PgConnectionString);

            foreach (var listing in businessListing)
            {
                try
                {
                    if (listing.LocationCode != "")
                    {
                        var locationCode = listing.LocationCode;
                        var locationSource = listing.DataSource;

                        var checkQuery =
                            $"Select * from business where locationId = '{locationCode}' and locationSource ='{locationSource}' ;";
                        var locationExists = connection.Query<DataModel.BusinessDTO>(checkQuery).ToList();

                        if (locationExists.Any())
                        {
                            var existingJson = locationExists.FirstOrDefault()?.locationDetails;
                            var locationJson = JsonConvert.SerializeObject(listing);

                            if (!CompareJson(existingJson, locationJson))
                            {
                                var updateQuery =
                                    $"update business set locationDetails='{locationJson}'  where locationId = '{locationCode}' and locationSource ='{locationSource}' ;";
                                connection.Query<DataModel.BusinessDTO>(updateQuery);

                                listing.NeedUpdate = true;
                                CommonHelper.UpdateLocationLog(locationDetails, listing.LocationCode, "location",
                                    "NEED-UPDATE", "");
                            }
                            else
                            {
                                listing.NeedUpdate = false;
                                CommonHelper.UpdateLocationLog(locationDetails, listing.LocationCode, "location",
                                    "NOT-REQUIRED", "");
                            }
                        }
                        else
                        {
                            var json = JsonConvert.SerializeObject(listing);
                            var insertQuery =
                                $"Insert into business (locationId, locationSource, locationDetails) values ('{locationCode}', '{locationSource}', '{json}');";
                            connection.Execute(insertQuery);
                            listing.NeedUpdate = true;
                            CommonHelper.UpdateLocationLog(locationDetails, listing.LocationCode, "location",
                                "NEW-ENTRY", "");
                        }
                    }
                    else
                    {
                        listing.NeedUpdate = false;
                        CommonHelper.UpdateLocationLog(locationDetails, listing.LocationCode, "location",
                            "NOT-REQUIRED", "");
                    }
                }
                catch (Exception ex)
                {
                    CommonHelper.UpdateLocationLog(locationDetails, listing.LocationCode, "location", "DBUPDATE-ERROR",
                        ex.Message);

                    Logger.FileLogger(ex.Message, Parameters.LogFile.LogFile.ToString());
                    Logger.FileLogger(ex, Parameters.LogFile.ErrorLogFile.ToString());
                }
            }
        }

        private static bool CompareJson(string sourceJsonString, string targetJsonString)
        {
            var sourceJObject = JsonConvert.DeserializeObject<JObject>(sourceJsonString);
            var targetJObject = JsonConvert.DeserializeObject<JObject>(targetJsonString);

            if (!JToken.DeepEquals(sourceJObject, targetJObject))
            {
                foreach (KeyValuePair<string, JToken> sourceProperty in sourceJObject)
                {
                    var targetProp = targetJObject.Property(sourceProperty.Key);

                    Console.WriteLine(!JToken.DeepEquals(sourceProperty.Value, targetProp.Value)
                        ? $"{sourceProperty.Key} property value is changed"
                        : $"{sourceProperty.Key} property value didn't change");
                }
            }
            else
            {
                return true;
            }

            return false;
        }
    }
}