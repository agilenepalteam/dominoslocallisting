﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using static AppleAPITest.Model.AppleBusinessDataModel;

namespace AppleAPITest.Helper
{
	public class AppleBusinessHelper
	{





		public static AppleBusinessDTO GetAppleBusiness1(AppleBusinessDetails businessDetails)
		{
			var appleBusiness = new AppleBusinessDTO()
			{
				//id = "1571493054114430976",
				businessDetails = businessDetails
			};
			return appleBusiness;
		}

		public static AppleBusinessDTO GetAppleBusiness2(AppleBusinessDetails businessDetails)
		{
			var appleBusiness = new AppleBusinessDTO()
			{
				//id = "1530286760244084736",
				businessDetails = businessDetails
			};
			return appleBusiness;
		}

		public static AppleBusinessDTO GetAppleBusiness3(AppleBusinessDetails businessDetails)
		{
			var appleBusiness = new AppleBusinessDTO()
			{
				//id = "1530079702340337664",
				businessDetails = businessDetails
			};
			return appleBusiness;
		}

		public static AppleBusinessDTO GetAppleBusiness4(AppleBusinessDetails businessDetails)
		{
			var appleBusiness = new AppleBusinessDTO()
			{
				//id = "1536115752534212608",
				businessDetails = businessDetails
			};
			return appleBusiness;
		}

		public static AppleBusinessDTO GetAppleBusiness5(AppleBusinessDetails businessDetails)
		{
			var appleBusiness = new AppleBusinessDTO()
			{
				//id = "1520962276855021568",
				businessDetails = businessDetails
			};
			return appleBusiness;
		}

		public static AppleBusinessDTO GetAppleBusiness6(AppleBusinessDetails businessDetails)
		{
			var appleBusiness = new AppleBusinessDTO()
			{
				//id = "1539514394038272000",
				businessDetails = businessDetails
			};
			return appleBusiness;
		}

		public static AppleBusinessDTO GetAppleBusiness7(AppleBusinessDetails businessDetails)
		{
			var appleBusiness = new AppleBusinessDTO()
			{
				//id = "1564944137267314688",
				businessDetails = businessDetails
			};
			return appleBusiness;
		}

		public static AppleBusinessDetails GetCreateAppleBusiness1()
		{
			var businessDetails = new AppleBusinessDetails()
			{
				partnersBusinessId = "RD002",
				partnersBusinessVersion = "RD002001",
				countryCodes = new List<string> { "AU" },
				displayNames = new List<AppleDisplayNames>()
				{
					new AppleDisplayNames()
					{
						name = "Resolution Digital",
						locale = "en-AU",
						primary = true
					}
				},
				categories = new List<string>() { "shopping.media" },
				urls = new List<AppleURLs>()
				{
					new AppleURLs()
					{
						url = "http://resolutiondigital.com.au/",
						type = "HOMEPAGE"
					}
				}
			};

			return businessDetails;
		}

		public static AppleBusinessDetails GetCreateAppleBusiness2()
		{
			var businessDetails = new AppleBusinessDetails()
			{
				partnersBusinessId = "MD001",
				partnersBusinessVersion = "MD001001",
				countryCodes = new List<string> { "AU" },
				displayNames = new List<AppleDisplayNames>()
				{
					new AppleDisplayNames()
					{
						name = "McDonald",
						locale = "en-AU",
						primary = true
					}
				},
				categories = new List<string>() { "restaurants.burgers" },
				urls = new List<AppleURLs>()
				{
					new AppleURLs()
					{
						url = "https://www.mcdonalds.com/",
						type = "HOMEPAGE"
					}
				}
			};

			return businessDetails;
		}

		public static AppleBusinessDetails GetCreateAppleBusiness3()
		{
			var businessDetails = new AppleBusinessDetails()
			{
				partnersBusinessId = "DO001",
				partnersBusinessVersion = "DO001001",
				countryCodes = new List<string> { "NZ" },
				displayNames = new List<AppleDisplayNames>()
				{
					new AppleDisplayNames()
					{
						name = "Domino's Pizza",
						locale = "en-NZ",
						primary = true
					}
				},
				categories = new List<string>() { "restaurants.pizza" },
				urls = new List<AppleURLs>()
				{
					new AppleURLs()
					{
						url = "https://www.dominos.co.nz/",
						type = "HOMEPAGE"
					}
				}
			};

			return businessDetails;
		}

		public static AppleBusinessDetails GetCreateAppleBusiness4()
		{
			var businessDetails = new AppleBusinessDetails()
			{
				partnersBusinessId = "MY001",
				partnersBusinessVersion = "MY001001",
				countryCodes = new List<string> { "AU" },
				displayNames = new List<AppleDisplayNames>()
				{
					new AppleDisplayNames()
					{
						name = "Mycar Tyre",
						locale = "en-AU",
						primary = true
					}
				},
				categories = new List<string>() { "auto.autopartssupplies", "auto.carwash" },
				urls = new List<AppleURLs>()
				{
					new AppleURLs()
					{
						url = "https://www.mycar.com.au/",
						type = "HOMEPAGE"
					}
				}
			};

			return businessDetails;
		}

		public static AppleBusinessDetails GetCreateAppleBusiness5()
		{
			var businessDetails = new AppleBusinessDetails()
			{
				partnersBusinessId = "PP001",
				partnersBusinessVersion = "PP001001",
				countryCodes = new List<string> { "AU" },
				displayNames = new List<AppleDisplayNames>()
				{
					new AppleDisplayNames()
					{
						name = "Priceline Pharmacy",
						locale = "en-AU",
						primary = true
					}
				},
				categories = new List<string>() { "health.pharmacy" },
				urls = new List<AppleURLs>()
				{
					new AppleURLs()
					{
						url = "https://www.priceline.com.au/",
						type = "HOMEPAGE"
					}
				}
			};

			return businessDetails;
		}

		public static AppleBusinessDetails GetCreateAppleBusiness6()
		{
			var businessDetails = new AppleBusinessDetails()
			{
				partnersBusinessId = "RD001",
				partnersBusinessVersion = "RD001001",
				countryCodes = new List<string> { "AU" },
				displayNames = new List<AppleDisplayNames>()
				{
					new AppleDisplayNames()
					{
						name = "Resolution Digital",
						locale = "en-AU",
						primary = true
					}
				},
				categories = new List<string>() { "shopping.media" },
				urls = new List<AppleURLs>()
				{
					new AppleURLs()
					{
						url = "http://resolutiondigital.com.au/",
						type = "HOMEPAGE"
					}
				}
			};

			return businessDetails;
		}

		public static AppleBusinessDetails GetCreateAppleBusiness7()
		{
			var businessDetails = new AppleBusinessDetails()
			{
				partnersBusinessId = "RD001",
				partnersBusinessVersion = "RD001V01",
				countryCodes = new List<string> { "AU" },
				displayNames = new List<AppleDisplayNames>()
				{
					new AppleDisplayNames()
					{
						name = "Resolution Digital - Australia",
						locale = "en-AU",
						primary = true
					}
				},
				categories = new List<string>() { "shopping.media" },
				urls = new List<AppleURLs>()
				{
					new AppleURLs()
					{
						url = "http://resolutiondigital.com.au/",
						type = "HOMEPAGE"
					}
				}
			};

			return businessDetails;
		}

		public static AppleMedia GetAppleMedia1()
		{
			var mediaDetails = new AppleMedia()
			{
				imageDetails = new ImageDetails() { url = "https://dev-flightdeck.resolutiondigital.com.au/Slide1.JPG" }
			};
			return mediaDetails;
		}

		public static AppleMedia GetAppleMedia2()
		{
			var mediaDetails = new AppleMedia()
			{
				imageDetails = new ImageDetails() { url = "https://dev-flightdeck.resolutiondigital.com.au/Slide2.JPG" }
			};
			return mediaDetails;
		}

		public static AppleMedia GetAppleMedia3()
		{
			var mediaDetails = new AppleMedia()
			{
				imageDetails = new ImageDetails() { url = "https://dev-flightdeck.resolutiondigital.com.au/Slide3.JPG" }
			};
			return mediaDetails;
		}

		public static AppleMedia GetAppleMedia4()
		{
			var mediaDetails = new AppleMedia()
			{
				imageDetails = new ImageDetails() { url = "https://dev-flightdeck.resolutiondigital.com.au/Slide4.JPG" }
			};
			return mediaDetails;
		}

		public static AppleMedia GetAppleMedia5()
		{
			var mediaDetails = new AppleMedia()
			{
				imageDetails = new ImageDetails() { url = "https://dev-flightdeck.resolutiondigital.com.au/Slide5.JPG" }
			};
			return mediaDetails;
		}

		public static AppleMedia GetAppleMedia6()
		{
			var mediaDetails = new AppleMedia()
			{
				imageDetails = new ImageDetails() { url = "https://dev-flightdeck.resolutiondigital.com.au/Slide6.JPG" }
			};
			return mediaDetails;
		}

		public static AppleMedia GetAppleMedia7()
		{
			var mediaDetails = new AppleMedia()
			{
				imageDetails = new ImageDetails() { url = "https://dev-flightdeck.resolutiondigital.com.au/Slide7.JPG" }
			};
			return mediaDetails;
		}

		public static AppleMedia GetAppleMedia8()
		{
			var mediaDetails = new AppleMedia()
			{
				imageDetails = new ImageDetails() { url = "https://dev-flightdeck.resolutiondigital.com.au/Slide8.JPG" }
			};
			return mediaDetails;
		}

		public static AppleMedia GetAppleMedia9()
		{
			var mediaDetails = new AppleMedia()
			{
				imageDetails = new ImageDetails() { url = "https://dev-flightdeck.resolutiondigital.com.au/Slide9.JPG" }
			};
			return mediaDetails;
		}

		public static AppleMedia GetAppleMedia10()
		{
			var mediaDetails = new AppleMedia()
			{
				imageDetails = new ImageDetails()
					{ url = "https://dev-flightdeck.resolutiondigital.com.au/Slide10.JPG" }
			};
			return mediaDetails;
		}

		public static AppleMedia GetAppleMedia11()
		{
			var mediaDetails = new AppleMedia()
			{
				imageDetails = new ImageDetails()
					{ url = "https://dev-flightdeck.resolutiondigital.com.au/Slide11.JPG" }
			};
			return mediaDetails;
		}

		public static AppleMedia GetAppleMedia12()
		{
			var mediaDetails = new AppleMedia()
			{
				imageDetails = new ImageDetails()
					{ url = "https://dev-flightdeck.resolutiondigital.com.au/Slide12.JPG" }
			};
			return mediaDetails;
		}

		public static AppleMedia GetAppleMedia13()
		{
			var mediaDetails = new AppleMedia()
			{
				imageDetails = new ImageDetails()
					{ url = "https://dev-flightdeck.resolutiondigital.com.au/Slide13.JPG" }
			};
			return mediaDetails;
		}

		public static AppleMedia GetAppleMedia14()
		{
			var mediaDetails = new AppleMedia()
			{
				imageDetails = new ImageDetails()
					{ url = "https://dev-flightdeck.resolutiondigital.com.au/Slide14.JPG" }
			};
			return mediaDetails;
		}

		public static AppleMedia GetAppleMedia15()
		{
			var mediaDetails = new AppleMedia()
			{
				imageDetails = new ImageDetails()
					{ url = "https://dev-flightdeck.resolutiondigital.com.au/Slide15.JPG" }
			};
			return mediaDetails;
		}

		public static AppleMedia GetAppleMedia16()
		{
			var mediaDetails = new AppleMedia()
			{
				imageDetails = new ImageDetails()
					{ url = "https://dev-flightdeck.resolutiondigital.com.au/Slide16.JPG" }
			};
			return mediaDetails;
		}

		public static async System.Threading.Tasks.Task<string> CreateAppleBusinessAsync(AppleBusinessDTO appleBusines,
			string accessToken)
		{
			// Replace with your API endpoint
			string apiUrl =
				"https://data-qualification.businessconnect.apple.com/api/v1/companies/1480446281775277056/businesses";

			// Replace with your Bearer token
			string bearerToken = accessToken;

			// Replace with your JSON data
			string jsonData = JsonConvert.SerializeObject(appleBusines);

			try
			{
				using (HttpClient client = new HttpClient())
				{
					// Set the Bearer token in the Authorization header
					client.DefaultRequestHeaders.Authorization =
						new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", bearerToken);

					// Set the content type to JSON
					client.DefaultRequestHeaders.Accept.Add(
						new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));

					// Create the HTTP request message with the JSON data
					HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Post, apiUrl);
					request.Content = new StringContent(jsonData, Encoding.UTF8, "application/json");

					// Send the POST request
					HttpResponseMessage response = await client.SendAsync(request);

					// Check if the request was successful (status code 200 OK)
					if (response.IsSuccessStatusCode)
					{
						// Read and print the JSON response
						string jsonResponse = await response.Content.ReadAsStringAsync();

						JObject json = JObject.Parse(jsonResponse);

						// Extract the access_token
						string id = json["id"]?.ToString();
						return id;

						return jsonResponse;
					}
					else
					{
						string jsonResponse = await response.Content.ReadAsStringAsync();
						Console.WriteLine($"Error: {response.StatusCode} - {response.ReasonPhrase}");
					}
				}
			}
			catch (Exception ex)
			{
				Console.WriteLine($"Exception: {ex.Message}");
			}

			return "json";
		}

//		public static async System.Threading.Tasks.Task<string> UpdateAppleBusinessAsync(AppleBusinessDTO appleBusines, string accessToken)
//		{

//					var keyValuePairs = new Dictionary<string, string>() {
//	{ "1571493054114430976", "6f60eb7b-aa1d-4001-b304-e50792e4a47b" },
//	{ "1530286760244084736", "c623cd96-269a-42fa-b648-3be4aa447ee5" },
//	{ "1530079702340337664", "c3c6d042-b6d8-4169-83a7-8f952892506b" },
//	{ "1536115752534212608", "43537418-c887-4df2-8e6f-58b84b789703" },
//	{ "1520962276855021568", "21a5b7cc-2354-41c5-adf5-9d1f6f9582a0" },
//	{ "1539514394038272000", "b7424977-6de3-47db-b082-0ffa562b1f62" },
//	{ "1564944137267314688", "084e1419-b4d4-4ef5-a343-6e13a975977a" }
//};



//		// Replace with your API endpoint
//		string apiUrl = "https://api-qualification.businessconnect.apple.com/api/v1/companies/1480446281775277056/businesses/" + appleBusines.id;

//			// Replace with your Bearer token
//			string bearerToken = accessToken;

//			// Replace with your JSON data
//			string jsonData = JsonConvert.SerializeObject(appleBusines);

//			try
//			{
//				using (HttpClient client = new HttpClient())
//				{
//					// Set the Bearer token in the Authorization header
//					client.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", bearerToken);

//					// Set the content type to JSON
//					client.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));

//					//client.DefaultRequestHeaders.Add("if-match", @"6f60eb7b-aa1d-4001-b304-e50792e4a47b");

//					var etag = keyValuePairs[appleBusines.id].ToString();

//					if (Guid.TryParse(etag, out Guid guidValue))
//					{
//						string formattedGuid = $"\"{guidValue}\"";
//						client.DefaultRequestHeaders.TryAddWithoutValidation("if-match", etag);
//						//client.DefaultRequestHeaders.TryAddWithoutValidation("if-match", "6f60eb7b-aa1d-4001-b304-e50792e4a47b");
//					}
//					else
//					{
//						Console.WriteLine("Invalid GUID format for 'if-match'");
//					}

//					//client.DefaultRequestHeaders.Add("if-match", "sad");
//					//client.DefaultRequestHeaders.Add("if-match", "c448a4ac0e354da496ba4f72ed64365f");
//					//client.DefaultRequestHeaders.Add("if-match", "c448a4ac-0e35-4da4-96ba-4f72ed64365f");

//					// Create the HTTP request message with the JSON data
//					HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Put, apiUrl);
//					request.Content = new StringContent(jsonData, Encoding.UTF8, "application/json");

//					// Send the POST request
//					HttpResponseMessage response = await client.SendAsync(request);

//					// Check if the request was successful (status code 200 OK)
//					if (response.IsSuccessStatusCode)
//					{
//						// Read and print the JSON response
//						string jsonResponse = await response.Content.ReadAsStringAsync();
//						return jsonResponse;
//						Console.WriteLine("JSON Response:");
//						Console.WriteLine(jsonResponse);
//					}
//					else
//					{
//						Console.WriteLine($"Error: {response.StatusCode} - {response.ReasonPhrase}");
//					}
//				}
//			}
//			catch (Exception ex)
//			{
//				Console.WriteLine($"Exception: {ex.Message}");
//			}

//			return "json";
//		}

		//public static async System.Threading.Tasks.Task<string> DeleteAppleBusinessAsync(AppleBusinessDTO appleBusines, string accessToken)
		//{
		//	var appleBusinessDetails = await GetAppleBusinessDetailsAsync(appleBusines.id, accessToken);

		//	// Replace with your API endpoint
		//	string apiUrl = "https://api-qualification.businessconnect.apple.com/api/v1/companies/1480446281775277056/businesses/" + appleBusines.id;

		//	// Replace with your Bearer token
		//	string bearerToken = accessToken;

		//	// Replace with your JSON data
		//	string jsonData = JsonConvert.SerializeObject(appleBusines);

		//	try
		//	{
		//		using (HttpClient client = new HttpClient())
		//		{
		//			// Set the Bearer token in the Authorization header
		//			client.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", bearerToken);

		//			// Set the content type to JSON
		//			client.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));

		//			//client.DefaultRequestHeaders.Add("if-match", @"6f60eb7b-aa1d-4001-b304-e50792e4a47b");

		//			var etag = appleBusinessDetails.etag;

		//			if (Guid.TryParse(etag, out Guid guidValue))
		//			{
		//				client.DefaultRequestHeaders.TryAddWithoutValidation("if-match", etag);
		//			}
		//			else
		//			{
		//				Console.WriteLine("Invalid GUID format for 'if-match'");
		//			}

		//			//client.DefaultRequestHeaders.Add("if-match", "sad");
		//			//client.DefaultRequestHeaders.Add("if-match", "c448a4ac0e354da496ba4f72ed64365f");
		//			//client.DefaultRequestHeaders.Add("if-match", "c448a4ac-0e35-4da4-96ba-4f72ed64365f");

		//			// Create the HTTP request message with the JSON data
		//			HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Delete, apiUrl);
		//			request.Content = new StringContent(jsonData, Encoding.UTF8, "application/json");

		//			// Send the POST request
		//			HttpResponseMessage response = await client.SendAsync(request);

		//			// Check if the request was successful (status code 200 OK)
		//			if (response.IsSuccessStatusCode)
		//			{
		//				// Read and print the JSON response
		//				string jsonResponse = await response.Content.ReadAsStringAsync();

		//				//AppleBusinessDetails jsonData1 = JsonConvert.DeserializeObject<AppleBusinessDetails>(jsonResponse);
		//				return jsonResponse;
		//				Console.WriteLine("JSON Response:");
		//				Console.WriteLine(jsonResponse);
		//			}
		//			else
		//			{
		//				Console.WriteLine($"Error: {response.StatusCode} - {response.ReasonPhrase}");
		//			}
		//		}
		//	}
		//	catch (Exception ex)
		//	{
		//		Console.WriteLine($"Exception: {ex.Message}");
		//	}

		//	return "JSON";
		//}

		public static async System.Threading.Tasks.Task<AppleBusinessDTO> GetAppleBusinessDetailsAsync(
			string appleBusinesId, string accessToken)
		{

			// Replace with your API endpoint
			string apiUrl =
				"https://api-qualification.businessconnect.apple.com/api/v1/companies/1480446281775277056/businesses/" +
				appleBusinesId;

			// Replace with your Bearer token
			string bearerToken = accessToken;

			try
			{
				using (HttpClient client = new HttpClient())
				{
					// Set the Bearer token in the Authorization header
					client.DefaultRequestHeaders.Authorization =
						new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", bearerToken);

					// Set the content type to JSON
					client.DefaultRequestHeaders.Accept.Add(
						new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));

					HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, apiUrl);

					// Send the POST request
					HttpResponseMessage response = await client.SendAsync(request);

					// Check if the request was successful (status code 200 OK)
					if (response.IsSuccessStatusCode)
					{
						// Read and print the JSON response
						var jsonResponse = await response.Content.ReadAsStringAsync();
						AppleBusinessDTO jsonData1 = JsonConvert.DeserializeObject<AppleBusinessDTO>(jsonResponse);
						return jsonData1;

						//return jsonResponse;
						Console.WriteLine("JSON Response:");
						Console.WriteLine(jsonResponse);
					}
					else
					{
						Console.WriteLine($"Error: {response.StatusCode} - {response.ReasonPhrase}");
					}
				}
			}
			catch (Exception ex)
			{
				Console.WriteLine($"Exception: {ex.Message}");
			}

			return new AppleBusinessDTO();
		}

		public static async System.Threading.Tasks.Task<string> UploadImage(AppleMedia imageDetail, string accessToken)
		{

			// Replace with your API endpoint
			string apiUrl =
				"https://api-qualification.businessconnect.apple.com/api/v1/companies/1480446281775277056/images/import";

			// Replace with your Bearer token
			string bearerToken = accessToken;

			string jsonData = JsonConvert.SerializeObject(imageDetail);

			try
			{
				using (HttpClient client = new HttpClient())
				{
					// Set the Bearer token in the Authorization header
					client.DefaultRequestHeaders.Authorization =
						new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", bearerToken);

					// Set the content type to JSON
					client.DefaultRequestHeaders.Accept.Add(
						new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));

					HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Post, apiUrl);
					request.Content = new StringContent(jsonData, Encoding.UTF8, "application/json");
					// Send the POST request
					HttpResponseMessage response = await client.SendAsync(request);

					// Check if the request was successful (status code 200 OK)
					if (response.IsSuccessStatusCode)
					{
						// Read and print the JSON response
						var jsonResponse = await response.Content.ReadAsStringAsync();



						//return jsonResponse;
						Console.WriteLine("JSON Response:");
						Console.WriteLine(jsonResponse);
						return jsonResponse;
					}
					else
					{
						Console.WriteLine($"Error: {response.StatusCode} - {response.ReasonPhrase}");
					}
				}
			}
			catch (Exception ex)
			{
				Console.WriteLine($"Exception: {ex.Message}");
			}

			return "yt";
		}

		public static async System.Threading.Tasks.Task<string> GetMediaMetadataAsync(string metadataId,
			string accessToken)
		{

			// Replace with your API endpoint
			string apiUrl =
				"https://api-qualification.businessconnect.apple.com/api/v1/companies/1480446281775277056/images/" +
				metadataId + "/metadata";

			// Replace with your Bearer token
			string bearerToken = accessToken;

			try
			{
				using (HttpClient client = new HttpClient())
				{
					// Set the Bearer token in the Authorization header
					client.DefaultRequestHeaders.Authorization =
						new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", bearerToken);

					// Set the content type to JSON
					client.DefaultRequestHeaders.Accept.Add(
						new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));

					HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, apiUrl);

					// Send the POST request
					HttpResponseMessage response = await client.SendAsync(request);

					// Check if the request was successful (status code 200 OK)
					if (response.IsSuccessStatusCode)
					{
						// Read and print the JSON response
						var jsonResponse = await response.Content.ReadAsStringAsync();
						//AppleBusinessDTO jsonData1 = JsonConvert.DeserializeObject<AppleBusinessDTO>(jsonResponse);
						return jsonResponse;

						//return jsonResponse;
						Console.WriteLine("JSON Response:");
						Console.WriteLine(jsonResponse);
					}
					else
					{
						Console.WriteLine($"Error: {response.StatusCode} - {response.ReasonPhrase}");
					}
				}
			}
			catch (Exception ex)
			{
				Console.WriteLine($"Exception: {ex.Message}");
			}

			return "rer";
		}

		public static async System.Threading.Tasks.Task<string> CreateAppleLocationAsync(
			Model.AppleLocationDataModel.AppleLocationModel appleBusines, string accessToken)
		{
			// Replace with your API endpoint
			string apiUrl =
				"https://data-qualification.businessconnect.apple.com/api/v1/companies/1480446281775277056/locations";

			// Replace with your Bearer token
			string bearerToken = accessToken;

			// Replace with your JSON data
			var location = new Model.AppleLocationDataModel.AppleLocationDTO()
			{
				locationDetails = appleBusines
			};
			string jsonData = JsonConvert.SerializeObject(location);
			Console.WriteLine(jsonData);

			try
			{
				using (HttpClient client = new HttpClient())
				{
					// Set the Bearer token in the Authorization header
					client.DefaultRequestHeaders.Authorization =
						new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", bearerToken);

					// Set the content type to JSON
					client.DefaultRequestHeaders.Accept.Add(
						new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));

					// Create the HTTP request message with the JSON data
					HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Post, apiUrl);
					request.Content = new StringContent(jsonData, Encoding.UTF8, "application/json");

					// Send the POST request
					HttpResponseMessage response = await client.SendAsync(request);

					// Check if the request was successful (status code 200 OK)
					if (response.IsSuccessStatusCode)
					{
						// Read and print the JSON response
						string jsonResponse = await response.Content.ReadAsStringAsync();
						Console.WriteLine("JSON Response:");
						Console.WriteLine(jsonResponse);
						JObject json = JObject.Parse(jsonResponse);

						// Extract the access_token
						string id = json["id"]?.ToString();
						//get Id 

						var a = await UploadLocationAsset(accessToken, id);

						return jsonResponse;
					}
					else
					{
						string jsonResponse = await response.Content.ReadAsStringAsync();
						Console.WriteLine($"Error: {response.StatusCode} - {response.ReasonPhrase}");
					}
				}
			}
			catch (Exception ex)
			{
				Console.WriteLine($"Exception: {ex.Message}");
			}

			return "json";
		}

		public static async System.Threading.Tasks.Task<string> DeleteAllbusiness(string accessToken)
		{
			// Replace with your API endpoint
			string apiUrl = "https://data-qualification.businessconnect.apple.com/api/v1/companies/1480446281775277056/locations?ql=createdDate=gt=2022-06-30T00:00:00Z&limit=100";

			// Replace with your Bearer token
			string bearerToken = accessToken;

			try
			{
				using (HttpClient client = new HttpClient())
				{
					// Set the Bearer token in the Authorization header
					client.DefaultRequestHeaders.Authorization =
						new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", bearerToken);

					// Set the content type to JSON
					client.DefaultRequestHeaders.Accept.Add(
						new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));

					// Create the HTTP request message with the JSON data
					HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, apiUrl);
					

					// Send the POST request
					HttpResponseMessage response = await client.SendAsync(request);

					// Check if the request was successful (status code 200 OK)
					if (response.IsSuccessStatusCode)
					{
						// Read and print the JSON response
						string jsonResponse = await response.Content.ReadAsStringAsync();
						Console.WriteLine("JSON Response:");
						Console.WriteLine(jsonResponse);
						JObject json = JObject.Parse(jsonResponse);

						var ao = json["data"];

						foreach (var d in ao)
						{
							// Extract the access_token
							string id = d["id"]?.ToString();
							string etag = d["etag"]?.ToString();
							//get Id 

							//var a = await UploadLocationAsset(accessToken, id);

							var abc= DeleteLocations(accessToken, id, etag).GetAwaiter().GetResult(); ;
						}

						return jsonResponse;
					}
					else
					{
						string jsonResponse = await response.Content.ReadAsStringAsync();
						Console.WriteLine($"Error: {response.StatusCode} - {response.ReasonPhrase}");
					}
				}
			}
			catch (Exception ex)
			{
				Console.WriteLine($"Exception: {ex.Message}");
			}

			return "json";
		}

		public static async System.Threading.Tasks.Task<string> UploadLocationAsset(string accessToken,
			string locationId)
		{
			// Replace with your API endpoint
			string apiUrl =
				"https://data-qualification.businessconnect.apple.com/api/v1/companies/1480446281775277056/locations/" +
				locationId + "/assets";

			// Replace with your Bearer token
			string bearerToken = accessToken;

			// Replace with your JSON data

			var assetId = "A" + locationId + "01";
			string jsonData = @"{
  ""assetDetails"": {
    ""partnersAssetId"": ""LocationAsset01"",
    ""dateAdded"": ""2024-01-01T09:45:34.000Z"",
    ""capturedBy"": ""Official Photo"",
    ""intent"": ""GALLERY"",
    ""captions"": [
      {
        ""title"": ""McDonald Official Logo"",
        ""altText"": ""McDonald"",
        ""locale"": ""en-US""
      }
    ],
    ""source"": ""BUSINESS"",
        ""photos"": {
      ""xlarge"": {
        ""url"": ""https://dev-flightdeck.resolutiondigital.com.au/coles-express-logo-1600-1040.jpg"",
        ""pixelHeight"": 1040,
        ""pixelWidth"": 1600
      }
    }
  }
}";
			Console.WriteLine(jsonData);

			try
			{
				using (HttpClient client = new HttpClient())
				{
					// Set the Bearer token in the Authorization header
					client.DefaultRequestHeaders.Authorization =
						new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", bearerToken);

					// Set the content type to JSON
					client.DefaultRequestHeaders.Accept.Add(
						new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));

					// Create the HTTP request message with the JSON data
					HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Post, apiUrl);
					request.Content = new StringContent(jsonData, Encoding.UTF8, "application/json");

					// Send the POST request
					HttpResponseMessage response = await client.SendAsync(request);

					// Check if the request was successful (status code 200 OK)
					if (response.IsSuccessStatusCode)
					{
						// Read and print the JSON response
						string jsonResponse = await response.Content.ReadAsStringAsync();
						Console.WriteLine("JSON Response:");
						Console.WriteLine(jsonResponse);
						return jsonResponse;

						//get Id 



					}
					else
					{
						string jsonResponse = await response.Content.ReadAsStringAsync();
						Console.WriteLine($"Error: {response.StatusCode} - {response.ReasonPhrase}");
					}
				}
			}
			catch (Exception ex)
			{
				Console.WriteLine($"Exception: {ex.Message}");
			}

			return "json";
		}

		public static async System.Threading.Tasks.Task<string> DeleteLocations(string accessToken, string locationId, string etag)
		{
			// Replace with your API endpoint
			string apiUrl = "https://data-qualification.businessconnect.apple.com/api/v1/companies/1480446281775277056/locations/" + locationId;

			// Replace with your Bearer token
			string bearerToken = accessToken;

			// Replace with your JSON data

			try
			{
				using (HttpClient client = new HttpClient())
				{
					// Set the Bearer token in the Authorization header
					client.DefaultRequestHeaders.Authorization =
						new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", bearerToken);

					// Set the content type to JSON
					client.DefaultRequestHeaders.Accept.Add(
						new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));

					// Create the HTTP request message with the JSON data
					HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Delete, apiUrl);
					client.DefaultRequestHeaders.Accept.Add(
						new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));

					if (Guid.TryParse(etag, out Guid guidValue))
					{
						client.DefaultRequestHeaders.TryAddWithoutValidation("if-match", etag);
					}
					else
					{
						Console.WriteLine("Invalid GUID format for 'if-match'");
					}

					// Send the POST request
					HttpResponseMessage response = await client.SendAsync(request);

					// Check if the request was successful (status code 200 OK)
					if (response.IsSuccessStatusCode)
					{
						// Read and print the JSON response
						string jsonResponse = await response.Content.ReadAsStringAsync();
						Console.WriteLine("JSON Response:");
						Console.WriteLine(jsonResponse);
						return jsonResponse;

						//get Id 



					}
					else
					{
						string jsonResponse = await response.Content.ReadAsStringAsync();
						Console.WriteLine($"Error: {response.StatusCode} - {response.ReasonPhrase}");
					}
				}
			}
			catch (Exception ex)
			{
				Console.WriteLine($"Exception: {ex.Message}");
			}

			return "json";
		}

	}
}