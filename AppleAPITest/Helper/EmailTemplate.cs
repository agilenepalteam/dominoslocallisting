﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Mail;

namespace AppleAPITest.Helper
{
    public static class EmailTemplate
    {
        #region Email Message
        public const string EmptyResponse = @"JSON URL is returning empty response. Either the URL is invalid or data is Empty";
        public const string InvalidJson = @"Error occured while deserializing json probably because of invalid JSON format.";
        #endregion

        #region Email Template

        private const string MainTemplate = @"<!DOCTYPE html>
                                            <html>
                                            <head>
                                            <style>
                                            #detailTable {
                                              font-family: Arial, Helvetica, sans-serif;
                                              border-collapse: collapse;
                                              width: 100%;
                                            }
                                            
                                            #detailTable td, #detailTable th {
                                              border: 1px solid #ddd;
                                              padding: 8px;
                                            }
                                            
                                            #detailTable tr:nth-child(even){background-color: #f2f2f2;}
                                            
                                            #detailTable tr:hover {background-color: #ddd;}
                                            
                                            #detailTable th {
                                              padding-top: 12px;
                                              padding-bottom: 12px;
                                              text-align: left;
                                              background-color: #98AFC7;
                                              color: white;
                                            }
                                            </style>
                                            </head>
                                            <body>
                                            <div> 
                                            {MailBody}
                                            </div>                                            
                                            </body>
                                            </html>";

        private const string DetailTemplate = @"
                                             <p> {message} </p>
                                              <p> Total Count : {totalCount} </p>
                                              <p> Failed Count : {failedCount} </p>
                                             <table id=""detailTable"">
                                             <tr>
                                               <th>Location Code</th>
                                               <th>Location Name</th>  
            <th>Location Status</th>
            <th>Location Message</th>
            <th>Apple Status</th>
            <th>Apple Message</th>
            <th>GMB Status</th>
            <th>GMB Message</th>
                                             </tr>
                                             {row}
                                             </table>";

        #endregion


        public static string GetMailBody(string mailBodyType, string bodyMessage, string extraMessage = "",
            List<DataModel.LocationLogDetails> locationDetails = null)
        {
            var body = "";
            if (mailBodyType == "PlainBody")
            {
                body = MainTemplate.Replace("{MailBody}",
                    bodyMessage + (extraMessage == "" ? "" : " - " + extraMessage));
            }

            if (mailBodyType == "DetailBody")
            {
                var allRow = "";
                var row = @"<tr>
                                <td>{LocationCode}</td>
                                <td>{LocationName}</td>  
                                <td>{LocationStatus}</td>
                                <td>{LocationMessage}</td>
                                <td>{AppleStatus}</td>
                                <td>{AppleMessage}</td>
                                <td>{GMBStatus}</td>
                                <td>{GMBMessage}</td>
                             </tr>";
                foreach (var item in locationDetails)
                {
                    allRow = allRow +
                             row
                                 .Replace("{LocationCode}", item.LocationCode)
                                 .Replace("{LocationName}", item.LocationName)
                                 .Replace("{LocationStatus}", item.LocationStatus)
                                 .Replace("{LocationMessage}", item.LocationMessage)
                                 .Replace("{AppleStatus}", item.AppleStatus)
                                 .Replace("{AppleMessage}", item.AppleMessage)
                                 .Replace("{GMBStatus}", item.GMBStatus)
                                 .Replace("{GMBMessage}", item.GMBMessage);
                }

                var detailBody = DetailTemplate.Replace("{message}", bodyMessage).Replace("{totalCount}", "0")
                    .Replace("{failedCount}", "0").Replace("{row}", allRow);
                body = MainTemplate.Replace("{MailBody}", detailBody);
            }

            return body;
        }

        public static void SendEmail(string subject, string body)
        {
            try
            {
                //var email = new MimeMessage();
                //email.Sender = MailboxAddress.Parse("");
                //email.To.Add(MailboxAddress.Parse(emailRequest.To));
                //email.Subject = subject;// emailRequest.Subject;
                //var builder = new BodyBuilder { HtmlBody = body };
                //email.Body = builder.ToMessageBody();
                //SmtpClient smtp = new SmtpClient();
                //// change third parameter to true for enabling SSL 
                //smtp.Connect(Configuration.SmtpClient, Configuration.Port.ToInt(), false);
                //smtp.Authenticate(Configuration.NetworkCredentialUsername, Configuration.NetworkCredentiaPassword);
                //smtp.Send(email);
                //smtp.Disconnect(true);

                try
                {
                    var sender = new MailAddress(Configuration.SmtpFromAddress,
                        Configuration.SmtpDisplayNameForFromAddress);
                    var replyTo = new MailAddress(Configuration.SmtpReplyAddress,
                        Configuration.SmtpDisplayNameForReplyAddress);

                    var mSmtpClient = new SmtpClient()
                    {
                        Host = Configuration.SmtpClient,
                        Port = Convert.ToInt32(Configuration.SmtpPort),
                        DeliveryMethod = SmtpDeliveryMethod.Network,
                        UseDefaultCredentials = false,
                        Credentials = new System.Net.NetworkCredential(Configuration.SmtpNetworkCredentialUsername,
                            Configuration.SmtpNetworkCredentialPassword),
                        EnableSsl = Configuration.SmtpEnableSsl == "TRUE" ? true : false,
                        Timeout = 30000
                    };

                    //create the MailMessage object
                    MailMessage mMailMessage = new MailMessage()
                    {
                        From = sender,
                        ReplyTo = replyTo,
                        Subject = subject,
                        Body = body,
                        IsBodyHtml = true
                    };

                    var splitRecipient = Configuration.Recipient.Split(';');
                    foreach (var item in splitRecipient)
                    {
                        mMailMessage.To.Add(item);
                    }

                    //create the SmtpClient instance
                    ServicePointManager.ServerCertificateValidationCallback += delegate { return true; };
                    //smtp.Send(message);
                    mSmtpClient.Send(mMailMessage);
                }
                catch (Exception ex)
                {
                    Logger.FileLogger(ex.Message, Parameters.LogFile.LogFile.ToString());
                    Logger.FileLogger(ex, Parameters.LogFile.ErrorLogFile.ToString());
                }
            }
            catch (Exception ex)
            {
                Logger.FileLogger($"Error occured while sending email. Error message is {ex.Message}",
                    Parameters.LogFile.LogFile.ToString());
            }
        }
    }
}
