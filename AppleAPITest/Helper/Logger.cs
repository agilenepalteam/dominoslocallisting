﻿using System;
using System.IO;
using System.Text;

namespace AppleAPITest.Helper
{
	public static class Logger
	{
		private static readonly object Locker = new object();

		/// <summary>
		/// Writes logs in file
		/// </summary>
		/// <param name="message"></param>
		/// <param name="fileName"></param>
		public static void FileLogger(string message, string fileName)
		{
			try
			{
				Console.WriteLine(message);

				var applicationPath = AppDomain.CurrentDomain.BaseDirectory;
				applicationPath = applicationPath + "\\" + "LOG";

				lock (Locker)
				{
					if (!Directory.Exists(applicationPath))
					{
						Directory.CreateDirectory(applicationPath);
					}

					var path = Path.Combine(applicationPath, fileName + ".log");
					if (!File.Exists(path))
					{
						File.Create(path).Close();
					}

					using (var writer = new StreamWriter(path, true))
					{
						writer.WriteLine(DateTime.Now + " -- " + message);
						writer.Close();
					}
				}
			}
			catch
			{
				//
			}
		}

		/// <summary>
		/// Writes exception in file
		/// </summary>
		/// <param name="ex"></param>
		/// <param name="fileName"></param>
		public static void FileLogger(Exception ex, string fileName)
		{
			try
			{
				Console.WriteLine(ex.Message);

				lock (Locker)
				{

					var applicationPath = AppDomain.CurrentDomain.BaseDirectory;
					applicationPath = applicationPath + "\\" + "LOG";

					if (!Directory.Exists(applicationPath))
					{
						Directory.CreateDirectory(applicationPath);
					}

					var path = Path.Combine(applicationPath, fileName + ".log");
					if (!File.Exists(path))
					{
						File.Create(path).Close();
					}

					using (var writer = new StreamWriter(path, true))
					{
						var type = ex.GetType();
						var builder = new StringBuilder();
						builder.AppendLine("------------------------------------");
						builder.AppendLine($"Timestamp: {DateTime.Now}");
						builder.AppendLine($"An exception of type \"{type.FullName}\" occured and was caught.");
						builder.AppendLine("------------------------------------");
						builder.AppendLine();

						var errCode = string.Empty;
						builder.AppendLine($"Type: {type.AssemblyQualifiedName}");
						if (!string.IsNullOrEmpty(errCode))
						{
							builder.AppendLine($"Inn {errCode}");
						}

						if (ex.InnerException != null)
						{
							builder.AppendLine($"Inner Exception: {ex.InnerException}");
						}

						builder.AppendLine($"Message: {ex.Message}");
						builder.AppendLine($"Source: {ex.Source}");
						builder.AppendLine($"Help Link: {ex.HelpLink}");
						builder.AppendLine($"Target Site: {ex.TargetSite}");
						builder.AppendLine($"Stack Trace: {ex.StackTrace}");
						builder.AppendLine();

						writer.WriteLine(builder.ToString());
						writer.Close();
					}
				}
			}
			catch
			{
				//
			}
		}
	}
}
