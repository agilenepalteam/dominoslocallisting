﻿using Google.Apis.Auth.OAuth2.Flows;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;
using Google.Apis.Auth.OAuth2;
using Google.Apis.MyBusinessAccountManagement.v1;
using Google.Apis.MyBusinessBusinessInformation.v1;
using Google.Apis.Services;
using AppleAPITest.Model;
using static AppleAPITest.Model.AppleLocationDataModel;
using Google.Apis.MyBusinessVerifications.v1;
using System.Net.Http;

namespace AppleAPITest.Helper
{
	public class MyData
	{
		public string id { get; set; }
		// Other properties if there are any
	}

	public class GMBHelper
	{
		public static MyBusinessAccountManagementService GetGMBAccount(string clientId, string clientSecret,
			string accessToken,
			string refreshToken)
		{
			try
			{
				var token = new Google.Apis.Auth.OAuth2.Responses.TokenResponse
				{
					AccessToken = accessToken,
					ExpiresInSeconds = 3600,
					Issued = DateTime.Now,
					RefreshToken = refreshToken,
					Scope = "https://www.googleapis.com/auth/plus.business.manage"
				};
				var fakeFlow = new GoogleAuthorizationCodeFlow(new GoogleAuthorizationCodeFlow.Initializer
					{ ClientSecrets = new ClientSecrets { ClientId = clientId, ClientSecret = clientSecret } });
				var c1 = new UserCredential(fakeFlow, "user", token);
				var serviceInitializer = new BaseClientService.Initializer
					{ ApplicationName = "Google My Business RESO", HttpClientInitializer = c1 };
				var myBusiness = new MyBusinessAccountManagementService(serviceInitializer);
				return myBusiness;
			}
			catch (Exception ex)
			{
				//WriteLog(ex.Message, FDGoogleMyBusinessLogType.FDGoogleMyBusinessReviewsError);
				//WriteLogException(ex, FDGoogleMyBusinessLogType.FDGoogleMyBusinessService);
				return null;
			}
		}

		public static MyBusinessBusinessInformationService GetGMBBusiness(string clientId, string clientSecret,
			string accessToken,
			string refreshToken)
		{
			try
			{
				var token = new Google.Apis.Auth.OAuth2.Responses.TokenResponse
				{
					AccessToken = accessToken,
					ExpiresInSeconds = 3600,
					Issued = DateTime.Now,
					RefreshToken = refreshToken,
					Scope = "https://www.googleapis.com/auth/plus.business.manage"
				};
				var fakeFlow = new GoogleAuthorizationCodeFlow(new GoogleAuthorizationCodeFlow.Initializer
					{ ClientSecrets = new ClientSecrets { ClientId = clientId, ClientSecret = clientSecret } });
				var c1 = new UserCredential(fakeFlow, "user", token);
				var serviceInitializer = new BaseClientService.Initializer
					{ ApplicationName = "Google My Business RESO", HttpClientInitializer = c1 };
				var myBusiness = new MyBusinessBusinessInformationService(serviceInitializer);
				return myBusiness;
			}
			catch (Exception ex)
			{
				//WriteLog(ex.Message, FDGoogleMyBusinessLogType.FDGoogleMyBusinessReviewsError);
				//WriteLogException(ex, FDGoogleMyBusinessLogType.FDGoogleMyBusinessService);
				return null;
			}
		}

		public static MyBusinessVerificationsService GetGMBVerifications(string clientId, string clientSecret,
	string accessToken,
	string refreshToken)
		{
			try
			{
				var token = new Google.Apis.Auth.OAuth2.Responses.TokenResponse
				{
					AccessToken = accessToken,
					ExpiresInSeconds = 3600,
					Issued = DateTime.Now,
					RefreshToken = refreshToken,
					Scope = "https://www.googleapis.com/auth/plus.business.manage"
				};
				var fakeFlow = new GoogleAuthorizationCodeFlow(new GoogleAuthorizationCodeFlow.Initializer
				{ ClientSecrets = new ClientSecrets { ClientId = clientId, ClientSecret = clientSecret } });
				var c1 = new UserCredential(fakeFlow, "user", token);
				var serviceInitializer = new BaseClientService.Initializer
				{ ApplicationName = "Google My Business RESO", HttpClientInitializer = c1 };
				var myBusiness = new MyBusinessVerificationsService(serviceInitializer);
				return myBusiness;
			}
			catch (Exception ex)
			{
				//WriteLog(ex.Message, FDGoogleMyBusinessLogType.FDGoogleMyBusinessReviewsError);
				//WriteLogException(ex, FDGoogleMyBusinessLogType.FDGoogleMyBusinessService);
				return null;
			}
		}

		public static List<DataModel.BusinessListingDTO> GetAllLocation(
			MyBusinessAccountManagementService myBusinessAcountService,
			MyBusinessBusinessInformationService myBusinessService, string dataSource1, List<string> category,
			string chain, List<DataModel.LocationLogDetails> locationDetails)
		{
			try
			{
				Logger.FileLogger("****** GETTING Account List *******", Parameters.LogFile.LogFile.ToString());

				var accountList = myBusinessAcountService.Accounts.List();
				var accountsResult = accountList.Execute();

				var listingObject = new List<DataModel.BusinessListingDTO>();

				if (accountsResult.Accounts != null)
				{
					foreach (var account in accountsResult.Accounts)
					{
						Logger.FileLogger("****** Account Name *******" + account.AccountName,
							Parameters.LogFile.LogFile.ToString());
						Logger.FileLogger("****** ############ *******" + account.AccountName,
							Parameters.LogFile.LogFile.ToString());

						var isGetAgain = false;
						var pageToken = "";

						getAgain:

						var locationsListRequest = myBusinessService.Accounts.Locations.List(account.Name);

						if (isGetAgain)
						{
							isGetAgain = false;
							locationsListRequest.PageToken = pageToken;
						}

						try
						{
							locationsListRequest.ReadMask =
								"name,languageCode,storeCode,title,phoneNumbers,categories,storefrontAddress,websiteUri,regularHours,specialHours,serviceArea,labels,adWordsLocationExtensions,latlng,openInfo,metadata,profile,relationshipData,moreHours,serviceItems";

							var locationsResult = locationsListRequest.Execute();

							pageToken = locationsResult.NextPageToken;

							if (locationsResult.Locations != null)
							{
								foreach (var l in locationsResult.Locations)
								{
									//var js = new JavaScriptSerializer().Serialize(l);

									try
									{
										Logger.FileLogger("****** Location Name *******" + l.Title,
											Parameters.LogFile.LogFile.ToString());
										Logger.FileLogger("****** $$$$$$$$$$$$$ *******" + l.Name,
											Parameters.LogFile.LogFile.ToString());

										var dataSource = dataSource1;
										var locationCode = l.StoreCode?.ToString().Trim();
										var locationName = l.Title?.ToString().Trim();
										var locationBusinessStatus = l.OpenInfo.Status.ToString();
										var addressGoogle = "";
										var locationAlternativeName = "";

										locationDetails.Add(new DataModel.LocationLogDetails()
											{ LocationCode = locationCode, LocationName = locationName });

										foreach (var al in l.StorefrontAddress.AddressLines)
										{
											addressGoogle = addressGoogle +
											                al.Replace("\r", " ").Replace("\n", " ").Trim() + " ";
										}

										var locationAddressStreetName =
											addressGoogle; // l.Address.AddressLines.ToString().ToString().Replace("\r", " ").Replace("\n", " ").Trim();
										var locationAddressStreetSuburb = l.StorefrontAddress?.Locality?.ToString()
											.Replace("\r", " ").Replace("\n", " ").Trim();
										var locationAddressStreetState = l.StorefrontAddress?.AdministrativeArea
											?.ToString().Replace("\r", " ").Replace("\n", " ").Trim();
										var locationAddressStreetPostalCode = l.StorefrontAddress?.PostalCode
											?.ToString().Replace("\r", " ").Replace("\n", " ").Trim();
										var locationGeoCoordinatesLongitude = l.Latlng?.Longitude.ToString().Trim();
										var locationGeoCoordinatesLatitude = l.Latlng?.Latitude.ToString().Trim();
										var locationUrl = l.WebsiteUri?.ToString().Trim();
										var locationPhoneNo = l.PhoneNumbers?.PrimaryPhone?.ToString().Trim()
											.Replace("(", "").Replace(")", "");

										if (locationPhoneNo != null)
										{
											if (locationPhoneNo.StartsWith("+"))
											{
												locationPhoneNo = locationPhoneNo.ToString()
													.Substring(1, locationPhoneNo.Length - 1);
											}

											if (locationPhoneNo.StartsWith("0"))
											{
												locationPhoneNo = locationPhoneNo.ToString()
													.Substring(1, locationPhoneNo.Length - 1);
											}

											if (locationPhoneNo.StartsWith("64") && locationPhoneNo.Length > 8)
											{
												locationPhoneNo = locationPhoneNo.ToString()
													.Substring(2, locationPhoneNo.Length - 2);
											}

											if (locationPhoneNo.StartsWith("61") && locationPhoneNo.Length > 8)
											{
												locationPhoneNo = locationPhoneNo.ToString()
													.Substring(2, locationPhoneNo.Length - 2);
											}

											locationPhoneNo = locationPhoneNo.Replace(" ", "").Replace("-", "");
										}
										else
										{
											locationPhoneNo = "";
										}

										var locationDescription = (l.Profile?.Description ?? "").Replace("\r", " ")
											.Replace("\n", " ");
										var locationOpeningHours = new List<DataModel.OpeningHoursDTO>();

										if (l.RegularHours != null)
										{
											foreach (var o in l.RegularHours.Periods)
											{
												var open = o.OpenDay;
												var close = o.CloseDay;

												if (o.OpenTime != null)
												{
													var openTimeHour = o.OpenTime?.Hours ?? 0;
													var openTimeMinute = o.OpenTime?.Minutes ?? 0;

													var closeTimeHour = o.CloseTime?.Hours ?? 0;
													var closeTimeMinute = o.CloseTime?.Minutes ?? 0;

													var day = CommonHelper.GetDayInt(open);
													var openTime1 =
														openTimeHour.ToString().PadLeft(2, '0') + ":" +
														openTimeMinute.ToString().PadLeft(2, '0');
													var closeTime1 =
														closeTimeHour.ToString().PadLeft(2, '0') + ":" +
														closeTimeMinute.ToString().PadLeft(2, '0');
													locationOpeningHours.Add(new DataModel.OpeningHoursDTO()
													{
														Day = day.ToString(), OpenTime = openTime1,
														CloseTime = closeTime1
													});
												}

												//var openTime = (o.OpenTime?.Hours ?? 0) + ":" + (o.OpenTime?.Minutes ?? 0);
												//var closeTime = o.CloseTime;

												//DateTime openDate = Convert.ToDateTime(open);
												//DateTime closeDate = Convert.ToDateTime(close);



												//var open = o.OpenDay;
												//var close =o.CloseDay;

												//DateTime openDate = Convert.ToDateTime(open);
												//DateTime closeDate = Convert.ToDateTime(close);

												//var day = CommonHelper.GetDayInt(o.OpenDay).ToString();
												//var openTime = o.OpenTime;
												//var closeTime = o.CloseTime;
												//;

												//locationOpeningHours.Add(new DataModel.OpeningHoursDTO()
												//{Day = day, OpenTime = openTime, CloseTime = closeTime});
											}
										}

										//Logger.FileLogger("****** " + locationCode + " / " + locationName,
										//	Parameters.LogFile.LogFile.ToString());

										//CREATE LISTING MODEL

										var locale = locationAddressStreetState == "NZ" ? "en-NZ" : "en-AU";

										var listing = new DataModel.BusinessListingDTO
											{ DataSource = dataSource, LocationCode = locationCode };
										listing.Locale = locale;

										var ln = new DataModel.StoreNameDTO
											{ Name = locationName, Locale = locale, Pronunciation = "" };
										listing.LocationName = new List<DataModel.StoreNameDTO>() { ln };

										if (locationAlternativeName.Trim() != "")
										{
											var aln = new DataModel.StoreNameDTO
											{
												Name = locationAlternativeName, Locale = "en-US", Pronunciation = ""
											};
											listing.AlternativeLocationName = new List<DataModel.StoreNameDTO>()
												{ aln };
										}
										else
										{
											listing.AlternativeLocationName = new List<DataModel.StoreNameDTO>();
										}

										var bs = new List<DataModel.BusinessStatusDTO>();
										if (locationBusinessStatus == "OPEN")
										{
											var n = new DataModel.BusinessStatusDTO { Status = "OPEN" };
											listing.BusinessStatus = new List<DataModel.BusinessStatusDTO>() { n };
										}
										else if (locationBusinessStatus == "CLOSED_PERMANENTLY")
										{
											DateTime? rd = null;
											rd = DateTime.Now.AddDays(-20);

											var n = new DataModel.BusinessStatusDTO
											{
												Status = "CLOSED_PERMANENTLY",
												CloseDate = rd
											};
											listing.BusinessStatus = new List<DataModel.BusinessStatusDTO>() { n };
										}
										else if (locationBusinessStatus == "CLOSED_TEMPORARILY")
										{
											DateTime? rd = null;
											if (l.OpenInfo.CanReopen == true)
											{
												rd = l.OpenInfo.OpeningDate == null
													? DateTime.Now.AddDays(20)
													: Convert.ToDateTime(l.OpenInfo.OpeningDate);
											}
											else
											{
												rd = DateTime.Now.AddDays(20);
											}

											var n = new DataModel.BusinessStatusDTO
											{
												Status = "CLOSED_TEMPORARILY",
												CanReopen = l.OpenInfo.CanReopen ?? false,
												ReOpenDate = rd
											};
											listing.BusinessStatus = new List<DataModel.BusinessStatusDTO>() { n };
										}
										else
										{
											//CLOSED_TEMPORARILY
											listing.BusinessStatus = bs;
										}

										var address = new DataModel.PostalAddressDTO { FullAddress = "" };
										var sa = new DataModel.StructuredAddressDTO
										{
											AddressLines = new List<string>() { locationAddressStreetName },
											Floor = "",
											Neighborhood = "",
											Locality = locationAddressStreetSuburb,
											StateProvince = locationAddressStreetState,
											PostalCode = locationAddressStreetPostalCode,
											CountryCode = locationAddressStreetState == "NZ" ? "NZ" : "AU"
										};
										address.StructuredAddress = new List<DataModel.StructuredAddressDTO>() { sa };
										listing.PostalAddress = address;

										var ddp = new DataModel.DisplayPointDTO
										{
											Coordinates = new DataModel.CoordinatesDTO()
											{
												Latitude = locationGeoCoordinatesLongitude,
												Longitude = locationGeoCoordinatesLatitude
											},
											Source = "MANUALLY_PLACED"
										};
										listing.DisplayPoint = ddp;

										listing.HomePage = locationUrl;

										var poh = new DataModel.PhoneNumberDTO
											{ PhoneNumber = locationPhoneNo, Intent = "", Type = "Landline" };
										listing.PhoneNumber = poh;

										var cl = new List<DataModel.CategoryDTO>();
										foreach (var c in category)
										{
											var c12 = new DataModel.CategoryDTO() { Category = c };
											cl.Add(c12);
										}

										//var c111 = new DataModel.CategoryDTO() {Category = category};

										listing.Categories = cl;

										listing.OpeningHours = locationOpeningHours;

										listing.Chain = dataSource;
										var llll = new DataModel.ContentDTO()
											{ Locale = locale, Content = locationDescription };

										listing.Content = new List<DataModel.ContentDTO>() { llll };

										listingObject.Add(listing);





									}
									catch (Exception ex)
									{
										var storeCode = l.StoreCode?.ToString().Trim();
										CommonHelper.UpdateLocationLog(locationDetails, storeCode, "location",
											"PARSE-ERROR", ex.Message);

										var a = 1;
										//WriteLog(
										//	clientId +
										//	"- Google Execution of request failed: Sleeping for 5 seconds. Updating account to InActive - " +
										//	ex.Message, FDGoogleMyBusinessLogType.FDGoogleMyBusinessLocationError);
										//WriteLogException(ex, FDGoogleMyBusinessLogType.FDGoogleMyBusinessLocationError);
									}
								}

								if (locationsResult.NextPageToken != null)
								{
									isGetAgain = true;
									goto getAgain;
								}
							}
						}
						catch (Exception ex)
						{
							var a = 1;
						}
					}
				}

				Logger.FileLogger("****** Adding Location done.  *******", Parameters.LogFile.LogFile.ToString());

				var m = listingObject;
				return m;
			}
			catch (Exception ex)
			{
				return new List<DataModel.BusinessListingDTO>();
			}
		}

		public static void GetAllLocationAppleObject(MyBusinessAccountManagementService myBusinessAccountService, MyBusinessBusinessInformationService myBusinessService)
		{
			try
			{
				var appleAccessToken = AppleHelper.GetAppleAccessTokenAsync().GetAwaiter().GetResult();
				var allLocation = new List<AppleLocationDataModel.AppleLocationModel>();

				var accountList = myBusinessAccountService.Accounts.List();
				var accountsResult = accountList.Execute();

				if (accountsResult.Accounts != null)
				{
					foreach (var account in accountsResult.Accounts)
					{
						//if (account.AccountName != "Coles Express Locations")
						//{
						//	continue;
						//}

						Logger.FileLogger("****** Account Name *******" + account.AccountName, Parameters.LogFile.LogFile.ToString());
						Logger.FileLogger("****** ############ *******" + account.AccountName, Parameters.LogFile.LogFile.ToString());

						//Create Account
						var accountId = account.Name.Replace("accounts/", "");

						var businessCountryCode = "AU";
						var businessLocale = "en-AU";
						var categories = new List<string>() { "restaurants.hotdogs" };
						var website = "https://mcdonalds.com.au/";

						var businessDetails = new AppleBusinessDataModel.AppleBusinessDTO()
						{
							businessDetails = new AppleBusinessDataModel.AppleBusinessDetails()
							{
								partnersBusinessId = accountId,
								partnersBusinessVersion = accountId + "V001",
								countryCodes = new List<string> { businessCountryCode },
								displayNames = new List<AppleBusinessDataModel.AppleDisplayNames>()
								{
									new AppleBusinessDataModel.AppleDisplayNames()
									{
										name = account.AccountName,
										locale = businessLocale,
										primary = true
									}
								},
								categories = categories,
								urls = new List<AppleBusinessDataModel.AppleURLs>()
								{
									new AppleBusinessDataModel.AppleURLs()
									{
										url = website,
										type = "HOMEPAGE"
									}
								}
							}
						};
						//var businessId = AppleBusinessHelper.CreateAppleBusinessAsync(businessDetails, appleAccessToken)
						//	.Result;

						var businessId = "1554083916468682752";

						var isGetAgain = false;
						var pageToken = "";

						getAgain:

						var locationsListRequest = myBusinessService.Accounts.Locations.List(account.Name);

						if (isGetAgain)
						{
							isGetAgain = false;
							locationsListRequest.PageToken = pageToken;
						}

						try
						{
							locationsListRequest.ReadMask =
								"name,languageCode,storeCode,title,phoneNumbers,categories,storefrontAddress,websiteUri,regularHours,specialHours,serviceArea,labels,adWordsLocationExtensions,latlng,openInfo,metadata,profile,relationshipData,moreHours,serviceItems";

							var locationsResult = locationsListRequest.Execute();

							pageToken = locationsResult.NextPageToken;

							if (locationsResult.Locations != null)
							{
								foreach (var l in locationsResult.Locations)
								{
									//var appleAccessToken1 = AppleHelper.GetAppleAccessTokenAsync().GetAwaiter().GetResult();

									//var js = new JavaScriptSerializer().Serialize(l);

									try
									{
										Logger.FileLogger("****** Location Name *******" + l.Title,
											Parameters.LogFile.LogFile.ToString());
										Logger.FileLogger("****** $$$$$$$$$$$$$ *******" + l.Name,
											Parameters.LogFile.LogFile.ToString());

										var listing = new AppleLocationDataModel.AppleLocationModel();

										var locationId = l.Name.Replace("locations/", "");
										listing.partnersLocationId = locationId;
										listing.partnersLocationVersion = locationId + "V001";
										listing.businessId = businessId;

										var locale =
											l.StorefrontAddress?.RegionCode?.ToString().Replace("\r", " ")
												.Replace("\n", " ").Trim() == "NZ"
												? "en-NZ"
												: "en-AU";
										var country =
											l.StorefrontAddress?.RegionCode?.ToString().Replace("\r", " ")
												.Replace("\n", " ").Trim() == "NZ"
												? "NZ"
												: "AU";

										//display names
										var locationName = l.Title?.ToString().Trim();
										if (locationName.ToLower().Contains("mcdonald's") || locationName.ToLower().Contains("mcdonald’s"))
											locationName = "McDonald's";
										//change location name as instructed by Apple


										var displayNames = new AppleLocationDataModel.DisplayNames()
										{
											name = locationName,
											locale = locale,
											primary = true
										};
										listing.displayNames = new List<AppleLocationDataModel.DisplayNames>()
											{ displayNames };

										//Store Code
										var locationCode = l.StoreCode?.ToString().Trim();
										listing.storeCode = locationId;

										//Address
										var addressGoogle = "";
										foreach (var al in l.StorefrontAddress.AddressLines)
										{
											addressGoogle = addressGoogle +
											                al.Replace("\r", " ").Replace("\n", " ").Trim() + " ";
										}

										var locationAddressAddress = addressGoogle;
										var locationAddressState = l.StorefrontAddress?.AdministrativeArea?.ToString()
											.Replace("\r", " ").Replace("\n", " ").Trim();
										var locationAddressLocality = l.StorefrontAddress?.Locality?.ToString()
											.Replace("\r", " ").Replace("\n", " ").Trim();
										var locationAddressPostalCode = l.StorefrontAddress?.PostalCode?.ToString()
											.Replace("\r", " ").Replace("\n", " ").Trim();

										var mainAddress = new MainAddress()
										{
											fullAddress = locationAddressAddress + " " + locationAddressLocality + " " +
														  locationAddressPostalCode + " " + locationAddressState,
											locale = locale,
											structuredAddress = new StructuredAddress()
											{
												fullThoroughfare = locationAddressAddress,
												locality = locationAddressLocality,
												administrativeArea = locationAddressState,
												postCode = locationAddressPostalCode,
												countryCode = country
											}
										};
										listing.mainAddress = mainAddress;

										var internalNickname = new InternalNicknames()
										{
											locale = locale,
											name = locationAddressAddress
										};
										listing.internalNicknames = new List<InternalNicknames>() { internalNickname };

										//URLs
										var _website = l.WebsiteUri?.ToString().Trim();
										var locationUrl = string.IsNullOrEmpty(_website) ? website : _website;
										var url = new URL()
										{
											type = "HOMEPAGE",
											url = AppleHelper.GetModifiedURl(locationUrl)
										};
										listing.urls = new List<URL>() { url };

										//Description
										var locationDescription = (l.Profile?.Description ?? "").Replace("\r", " ")
											.Replace("\n", " ");
										var locationDescriptions = new LocationDescriptions()
										{
											descriptions = new List<LocationDescription>()
											{
												new LocationDescription()
												{
													locale = locale,
													text = locationDescription == ""
														? "The McDonald's store offers a range of delicious juicy burgers, wraps, sides and drinks. Don’t forget to try our milkshakes or just enjoy a cup of the finest coffee. Visit the store today and try all of your favourites"
														: locationDescription.Length >= 500
															? locationDescription.Substring(0, 498)
															: locationDescription
												}
											},
											type = "ABOUT"
										};
										listing.locationDescriptions = new List<LocationDescriptions>()
											{ locationDescriptions };

										//categories
										listing.categories = categories;

										//var googleCategories = l.Categories;
										//var cats = new List<string>();
										//if (googleCategories != null)
										//{
										//	if (googleCategories.PrimaryCategory != null)
										//	{
										//		var c1 = googleCategories.PrimaryCategory.DisplayName??"";
										//		var c2 = googleCategories.PrimaryCategory.Name ?? "";
										//		cats.Add(c1 + " ** " + c2);
										//	}
										//	if (googleCategories.AdditionalCategories != null)
										//	{
										//		foreach (var additional in googleCategories.AdditionalCategories) {
										//			var c1 = additional.DisplayName ?? "";
										//			var c2 = additional.Name ?? "";
										//			cats.Add(c1 + " ** " + c2);
										//		}
										//	}
										//}
										//if (cats.Count() > 0)
										//{
										//	listing.categories = cats;
										//}

										//display points
										var locationGeoCoordinatesLongitude = l.Latlng?.Longitude.ToString().Trim();
										var locationGeoCoordinatesLatitude = l.Latlng?.Latitude.ToString().Trim();

										if (!string.IsNullOrEmpty(locationGeoCoordinatesLongitude))
										{
											var ddp = new DisplayPoint
											{
												coordinates = new Coordinates()
												{
													latitude = locationGeoCoordinatesLatitude,
													longitude = locationGeoCoordinatesLongitude
												},
												source = "MANUALLY_PLACED"
											};
											listing.displayPoint = ddp;
										}
										else
										{
											//
										}

										//phone numbers
										var locationPhoneNo = l.PhoneNumbers?.PrimaryPhone?.ToString().Trim()
											.Replace("(", "").Replace(")", "");
										if (locationPhoneNo != null)
										{
											if (locationPhoneNo.StartsWith("+"))
											{
												locationPhoneNo = locationPhoneNo.ToString()
													.Substring(1, locationPhoneNo.Length - 1);
											}

											if (locationPhoneNo.StartsWith("0"))
											{
												locationPhoneNo = locationPhoneNo.ToString()
													.Substring(1, locationPhoneNo.Length - 1);
											}

											if (locationPhoneNo.StartsWith("64") && locationPhoneNo.Length > 8)
											{
												locationPhoneNo = locationPhoneNo.ToString()
													.Substring(2, locationPhoneNo.Length - 2);
											}

											if (locationPhoneNo.StartsWith("61") && locationPhoneNo.Length > 8)
											{
												locationPhoneNo = locationPhoneNo.ToString()
													.Substring(2, locationPhoneNo.Length - 2);
											}

											locationPhoneNo = locationPhoneNo.Replace(" ", "").Replace("-", "");

											if (country.ToLower() == "au")
											{
												locationPhoneNo = "+61" + locationPhoneNo;
											}
											else if (country.ToLower() == "nz")
											{
												locationPhoneNo = "+64" + locationPhoneNo;
											}

										}
										else
										{
											locationPhoneNo = "";
										}

										if (!string.IsNullOrEmpty(locationPhoneNo))
										{
											var poh = new PhoneNumber
												{ phoneNumber = locationPhoneNo, primary = true, type = "LANDLINE" };
											listing.phoneNumbers = new List<PhoneNumber>() { poh };
										}

										//location status
										var locationBusinessStatus = l.OpenInfo.Status.ToString();

										var bs = new List<LocationStatus>();
										if (locationBusinessStatus == "OPEN")
										{
											var n = new LocationStatus { status = "OPEN" };
											listing.locationStatus = n;
										}
										else if (locationBusinessStatus == "CLOSED")
										{
											DateTime? rd = null;
											rd = DateTime.Now.AddDays(-20);
											var n = new LocationStatus { status = "CLOSED ", closedDate = rd };
											listing.locationStatus = n;
										}
										else if (locationBusinessStatus == "TEMPORARILY_CLOSED")
										{
											DateTime? rd = null;
											if (l.OpenInfo.CanReopen == true)
											{
												rd = l.OpenInfo.OpeningDate == null
													? DateTime.Now.AddDays(20)
													: Convert.ToDateTime(l.OpenInfo.OpeningDate);
											}
											else
											{
												rd = DateTime.Now.AddDays(20);
											}

											var n = new LocationStatus
											{
												status = "CLOSED_TEMPORARILY",
												reopenDate = rd
											};
											listing.locationStatus = n;
										}
										else
										{
											var n = new LocationStatus { status = "OPEN" };
											listing.locationStatus = n;
										}

										//Opening Hours
										var locationOpeningHours = new List<OpeningHoursByDay>();


										if (l.RegularHours != null)
										{
											foreach (var o in l.RegularHours.Periods)
											{
												var open = o.OpenDay;
												var close = o.CloseDay;

												if (o.OpenTime != null)
												{
													var openTimeHour = o.OpenTime?.Hours ?? 0;
													var openTimeMinute = o.OpenTime?.Minutes ?? 0;

													var closeTimeHour = o.CloseTime?.Hours ?? 0;
													var closeTimeMinute = o.CloseTime?.Minutes ?? 0;

													var day = open;
													var openTime1 =
														openTimeHour.ToString().PadLeft(2, '0') + ":" +
														openTimeMinute.ToString().PadLeft(2, '0');
													var closeTime1 =
														closeTimeHour.ToString().PadLeft(2, '0') + ":" +
														closeTimeMinute.ToString().PadLeft(2, '0');
													locationOpeningHours.Add(new OpeningHoursByDay()
													{
														day = day.ToString(),
														times = new List<Times>()
														{
															new Times() { startTime = openTime1, endTime = closeTime1 }
														}
													});
												}
											}

											listing.openingHoursByDay = locationOpeningHours;
										}

										Console.WriteLine(listing);
										allLocation.Add(listing);
										if (listing.displayPoint != null)
										{
											//UPLOAD TO APPLE
											//var retValue = AppleBusinessHelper
											//	.CreateAppleLocationAsync(listing, appleAccessToken).GetAwaiter()
											//	.GetResult();
										}
									}
									catch (Exception ex)
									{
										var storeCode = l.StoreCode?.ToString().Trim();
										//CommonHelper.UpdateLocationLog(locationDetails, storeCode, "location",
										//	"PARSE-ERROR", ex.Message);

										var a = 1;
										//WriteLog(
										//	clientId +
										//	"- Google Execution of request failed: Sleeping for 5 seconds. Updating account to InActive - " +
										//	ex.Message, FDGoogleMyBusinessLogType.FDGoogleMyBusinessLocationError);
										//WriteLogException(ex, FDGoogleMyBusinessLogType.FDGoogleMyBusinessLocationError);
									}
								}

								if (locationsResult.NextPageToken != null)
								{
									isGetAgain = true;
									goto getAgain;
								}
							}
						}
						catch (Exception ex)
						{
							var a = 1;
						}
					}
				}

				Logger.FileLogger("****** Adding Location done.  *******", Parameters.LogFile.LogFile.ToString());
			}
			catch (Exception ex)
			{
				//return new List<DataModel.BusinessListingDTO>();
			}
		}

		public static void GetAllLocationVerification(MyBusinessAccountManagementService myBusinessAccountService, MyBusinessBusinessInformationService myBusinessService, MyBusinessVerificationsService myBusinessVerificationService)
		{
			try
			{
				var accountList = myBusinessAccountService.Accounts.List();
				var accountsResult = accountList.Execute();

				if (accountsResult.Accounts != null)
				{
					foreach (var account in accountsResult.Accounts)
					{
						var isGetAgain = false;
						var pageToken = "";

						getAgain:

						var locationsListRequest = myBusinessService.Accounts.Locations.List(account.Name);

						if (isGetAgain)
						{
							isGetAgain = false;
							locationsListRequest.PageToken = pageToken;
						}

						try
						{
							locationsListRequest.ReadMask =
								"name,languageCode,storeCode,title,phoneNumbers,categories,storefrontAddress,websiteUri,regularHours,specialHours,serviceArea,labels,adWordsLocationExtensions,latlng,openInfo,metadata,profile,relationshipData,moreHours,serviceItems";

							var locationsResult = locationsListRequest.Execute();

							pageToken = locationsResult.NextPageToken;

							if (locationsResult.Locations != null)
							{
								foreach (var l in locationsResult.Locations)
								{

									try
									{
										var requestBody = new Google.Apis.MyBusinessVerifications.v1.Data.VerifyLocationRequest();

										var verificationRequest = myBusinessVerificationService.Locations.Verifications.List( l.Name);

										var verifyResult = verificationRequest.Execute();


									}
									catch (Exception ex)
									{
										var storeCode = l.StoreCode?.ToString().Trim();
										//CommonHelper.UpdateLocationLog(locationDetails, storeCode, "location",
										//	"PARSE-ERROR", ex.Message);

										var a = 1;
										//WriteLog(
										//	clientId +
										//	"- Google Execution of request failed: Sleeping for 5 seconds. Updating account to InActive - " +
										//	ex.Message, FDGoogleMyBusinessLogType.FDGoogleMyBusinessLocationError);
										//WriteLogException(ex, FDGoogleMyBusinessLogType.FDGoogleMyBusinessLocationError);
									}
								}

								if (locationsResult.NextPageToken != null)
								{
									isGetAgain = true;
									goto getAgain;
								}
							}
						}
						catch (Exception ex)
						{
							var a = 1;
						}
					}
				}

				Logger.FileLogger("****** Adding Location done.  *******", Parameters.LogFile.LogFile.ToString());
			}
			catch (Exception ex)
			{
				//return new List<DataModel.BusinessListingDTO>();
			}
		}

		public static void DeleteAppleObject()
		{
			var appleAccessToken = AppleHelper.GetAppleAccessTokenAsync().GetAwaiter().GetResult();
			var retValue = AppleBusinessHelper.DeleteAllbusiness(appleAccessToken).GetAwaiter().GetResult();
		}



		//public static string UpdateGMB(MyBusinessService myBusinessService, List<DataModel.BusinessListingDTO> businessListing, List<DataModel.LocationLogDetails> locationDetails)
		//{
		//    foreach (var listing in businessListing)
		//    {
		//        try
		//        {
		//            var accountType = "";
		//            var locationsListRequest = myBusinessService.Accounts.Locations.List(accountType);

		//            myBusinessService.Accounts.Locations.
		//            if (isGetAgain)
		//            {
		//                isGetAgain = false;
		//                locationsListRequest.PageToken = pageToken;
		//            }

		//            var locationsResult = locationsListRequest.Execute();


		//            if (listing.LocationCode != "")
		//            {
		//                appleJsonLine = appleJsonLine + "\"site_code\": \"" + listing.LocationCode + "\"";

		//                if (listing.LocationName.Any())
		//                {
		//                    var nameValue = "";
		//                    foreach (var name in listing.LocationName)
		//                    {
		//                        if (nameValue != "")
		//                        {
		//                            nameValue = nameValue + ",";
		//                        }

		//                        nameValue = nameValue + "{ \"name\": \"" + name.Name + "\", \"locale\": \"" +
		//                                    name.Locale +
		//                                    "\" }";
		//                    }

		//                    appleJsonLine = appleJsonLine + ",\"names\": [" + nameValue + "  ]";
		//                }

		//                if (listing.AlternativeLocationName.Any())
		//                {
		//                    var nameValue = "";
		//                    foreach (var name in listing.AlternativeLocationName)
		//                    {
		//                        if (nameValue != "")
		//                        {
		//                            nameValue = nameValue + ",";
		//                        }

		//                        nameValue = nameValue + "{ \"name\": \"" + name.Name + "\", \"locale\": \"" +
		//                                    name.Locale +
		//                                    "\" }";
		//                    }

		//                    appleJsonLine = appleJsonLine + ",\"alternate_names\": [" + nameValue + "  ]";
		//                }

		//                if (listing.BusinessStatus.Any())
		//                {
		//                    var bs = listing.BusinessStatus.FirstOrDefault();
		//                    if (bs.Status == "OPEN")
		//                    {
		//                        appleJsonLine = appleJsonLine + ",\"business_status\": { \"status\": \"Open\" } ";
		//                    }
		//                    else if (bs.Status == "CLOSED_PERMANENTLY")
		//                    {
		//                        var d = ((DateTime)bs.CloseDate).ToString("YYYY-MM-SS");
		//                        appleJsonLine = appleJsonLine +
		//                                        ",\"business_status\": { \"status\": \"Closed\", \"closed_date\": \"" +
		//                                        d + "\" } ";
		//                    }
		//                    else if (bs.Status == "CLOSED_TEMPORARILY")
		//                    {
		//                        var d = ((DateTime)bs.ReOpenDate).ToString("YYYY-MM-SS");
		//                        appleJsonLine = appleJsonLine +
		//                                        ",\"business_status\": { \"status\": \"Closed\", \"reopen_date\": \"" +
		//                                        d + "\" } ";
		//                    }

		//                }
		//                else
		//                {
		//                    appleJsonLine = appleJsonLine + ",\"business_status\": { \"status\": \"Open\" } ";
		//                }

		//                if (listing.PostalAddress != null)
		//                {
		//                    if (listing.PostalAddress.StructuredAddress.Any())
		//                    {
		//                        var address = listing.PostalAddress.StructuredAddress.FirstOrDefault();
		//                        if (address != null)
		//                        {
		//                            if (address.AddressLines.Any() || address.CountryCode != "" ||
		//                                address.Floor != "" ||
		//                                address.Locality != "" || address.Neighborhood != "" ||
		//                                address.PostalCode != "" ||
		//                                address.StateProvince != "")
		//                            {
		//                                appleJsonLine =
		//                                    appleJsonLine + ",\"main_address\": { \"structured_address\": { ";
		//                                appleJsonLine = appleJsonLine + "\"street_address\": [ \"" +
		//                                                address.AddressLines.FirstOrDefault() + "\" ]";
		//                                appleJsonLine = appleJsonLine + ",\"locality\": \"" + address.Locality + "\"";
		//                                appleJsonLine =
		//                                    appleJsonLine + ",\"state_province\": \"" + address.StateProvince +
		//                                    "\"";
		//                                appleJsonLine = appleJsonLine + ",\"country_code\": \"" + address.CountryCode +
		//                                                "\"";
		//                                appleJsonLine =
		//                                    appleJsonLine + ",\"postal_code\": \"" + address.PostalCode + "\"";
		//                                appleJsonLine = appleJsonLine + "}}";
		//                            }
		//                        }
		//                    }
		//                }

		//                if (listing.DisplayPoint != null)
		//                {

		//                    appleJsonLine = appleJsonLine + ",\"display_point\": {\"coordinates\": { \"latitude\": \"" +
		//                                    listing.DisplayPoint.Coordinates.Longitude + "\", \"longitude\": \"" +
		//                                    listing.DisplayPoint.Coordinates.Latitude + "\" },\"source\": \"" +
		//                                    listing.DisplayPoint.Source + "\"}";
		//                }

		//                if (listing.HomePage != null)
		//                {
		//                    appleJsonLine = appleJsonLine + ",\"home_page\": \"" + listing.HomePage + "\"";
		//                }

		//                if (listing.PhoneNumber != null)
		//                {
		//                    appleJsonLine = appleJsonLine + ",\"phone_number\": { \"number\": \"" +
		//                                    listing.PhoneNumber.PhoneNumber + "\", \"type\": \"" +
		//                                    listing.PhoneNumber.Type + "\" } ";
		//                }

		//                if (listing.Categories.Any())
		//                {
		//                    var categoryString = "";
		//                    foreach (var category in listing.Categories)
		//                    {
		//                        if (categoryString != "")
		//                        {
		//                            categoryString = categoryString + ",";
		//                        }

		//                        categoryString = categoryString + "\"" + category.Category + "\"";
		//                    }

		//                    appleJsonLine = appleJsonLine + ",\"categories\": [" + categoryString + "]";
		//                }

		//                if (listing.OpeningHours.Any())
		//                {
		//                    var hourString = "";
		//                    foreach (var oh in listing.OpeningHours)
		//                    {
		//                        if (oh.Day != "")
		//                        {
		//                            hourString = hourString + CommonHelper.GetDayText(oh.Day) + " " + oh.OpenTime + "-" +
		//                                         oh.CloseTime + ";";
		//                        }
		//                    }

		//                    appleJsonLine = appleJsonLine + ",\"hours\": \"" + hourString + "\"";
		//                }

		//                if (listing.Chain.Any())
		//                {
		//                    var chainString = "";
		//                    foreach (var chain in listing.Chain)
		//                    {
		//                        if (chainString != "")
		//                        {
		//                            chainString = chainString + ",";
		//                        }

		//                        chainString = chainString + "\"" + chain + "\"";
		//                    }

		//                    appleJsonLine = appleJsonLine + ",\"chain\": [" + chainString + "]";
		//                }

		//                appleJsonLine = "{" + appleJsonLine + "}";

		//                appleJsonString = appleJsonString + appleJsonLine + Environment.NewLine;
		//            }
		//        }
		//        catch (Exception ex)
		//        {
		//            CommonHelper.UpdateLocationLog(locationDetails, listing.LocationCode, "apple", "JSON-ERROR", ex.Message);

		//            Logger.FileLogger(ex.Message, Parameters.LogFile.LogFile.ToString());
		//            Logger.FileLogger(ex, Parameters.LogFile.ErrorLogFile.ToString());
		//        }
		//    }

		//    return appleJsonString;
		//}
	}
}
