﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AppleAPITest.Model
{
	public class AppleBusinessDataModel
	{
		public class AppleDisplayNames
		{
			public string name { get; set; }
			public string locale { get; set; }
			public bool primary { get; set; }
		}
		public class AppleURLs
		{
			public string url { get; set; }
			public string type { get; set; }
		}

		public class AppleMedia
		{
			public ImageDetails imageDetails { get; set; }
		}

		public class ImageDetails
		{
			public string url { get; set; }
		}

		public class AppleBusinessDetails
		{
			public string partnersBusinessId { get; set; }
			public string partnersBusinessVersion { get; set; }
			public List<AppleDisplayNames> displayNames { get; set; }
			public List<string> categories { get; set; }
			public List<string> countryCodes { get; set; }
			public List<AppleURLs> urls { get; set; }

		}

		public class AppleBusinessDTO
		{
			//public string id { get; set; }
			//public string etag { get; set; }
			public AppleBusinessDetails businessDetails { get; set; }
		}
	}
}
