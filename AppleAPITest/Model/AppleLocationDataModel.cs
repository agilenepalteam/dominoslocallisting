﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AppleAPITest.Model
{
	public class AppleLocationDataModel
	{
		public class AppleLocationDTO
		{
			//public string id { get; set; }
			//public string etag { get; set; }
			public AppleLocationModel locationDetails { get; set; }
		}

		public class DisplayNames
		{
			public string name { get; set; }
			public string locale { get; set; }
			public bool primary { get; set; }

		}
		public class InternalNicknames
		{
			public string name { get; set; }
			public string locale { get; set; }
		}
		public class DisplayPoint
		{
			public Coordinates coordinates { get; set; }
			public string source { get; set; }
		}
		public class Coordinates
		{
			public string latitude { get; set; }
			public string longitude { get; set; }
		}
		public class LocationStatus
		{
			public string status { get; set; }
			public DateTime? closedDate { get; set; }
			public DateTime? reopenDate { get; set; }
		}
		public class MainAddress
		{
			public string fullAddress { get; set; }
			public StructuredAddress structuredAddress { get; set; }
			public string locale { get; set; }
		}
		public class StructuredAddress
		{
			public string unit { get; set; }
			public string floor { get; set; }
			public string building { get; set; }
			public string thoroughfare { get; set; }
			public string subThoroughfare { get; set; }
			public string fullThoroughfare { get; set; }
			public List<string> dependentLocality { get; set; }
			public string subLocality { get; set; }
			public string locality { get; set; }
			public string subAdministrativeArea { get; set; }
			public string administrativeArea { get; set; }
			public string postCode { get; set; }
			public string countryCode { get; set; }
		}
		public class OpeningHoursByDay
		{
			public string day { get; set; }
			public List<Times> times { get; set; }
		}
		public class Times
		{
			public string startTime { get; set; }
			public string endTime { get; set; }

		}
		public class PhoneNumber
		{
			public string phoneNumber { get; set; }
			public string type { get; set; }
			public bool primary { get; set; }
		}
		public class URL
		{
			public string url { get; set; }
			public string type { get; set; }
		}
		public class LocationAttribute
		{
			public string type { get; set; }
			public string provided { get; set; }
		}
		public class LocationDescriptions
		{
			public string type { get; set; }
			public List<LocationDescription> descriptions { get; set; }
		}
		public class LocationDescription
		{
			public string text { get; set; }
			public string locale { get; set; }
		}
		public class ServiceHours
		{
			public string category { get; set; }
			public List<OpeningHoursByDay> hoursByDay { get; set; }
		}
		public class SpecialHours
		{
			public List<OpeningHoursByDay> hoursByDay { get; set; }
			public DateTime startDate { get; set; }
			public DateTime endDate { get; set; }
			public bool closed { get; set; }
			public List<Description> descriptions { get; set; }
		}
		public class Description
		{
			public string text { get; set; }
			public string locale { get; set; }
		}
		public class AppleLocationModel
		{
			public string partnersLocationId { get; set; }
			public string partnersLocationVersion { get; set; }
			public string businessId { get; set; }
			public List<DisplayNames> displayNames { get; set; }
			public DisplayPoint displayPoint { get; set; }
			public LocationStatus locationStatus { get; set; }
			public MainAddress mainAddress { get; set; }
			public List<string> categories { get; set; }
			public List<OpeningHoursByDay> openingHoursByDay { get; set; }
			public List<PhoneNumber> phoneNumbers { get; set; }
			public List<URL> urls { get; set; }
			public List<InternalNicknames> internalNicknames { get; set; }
			public List<LocationAttribute> locationAttributes { get; set; }
			public List<LocationDescriptions> locationDescriptions { get; set; }
			public string storeCode { get; set; }
			public List<ServiceHours> serviceHours { get; set; }
			public List<SpecialHours> specialHours { get; set; }
		}
	}
}

