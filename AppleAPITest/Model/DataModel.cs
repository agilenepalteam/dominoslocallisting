﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AppleAPITest
{
	public class DataModel
	{
		public class BusinessListingDTO
		{
			public string DataSource { get; set; }
			public string LocationCode { get; set; }
			public List<StoreNameDTO> LocationName { get; set; }
			public List<StoreNameDTO> AlternativeLocationName { get; set; }
			public List<BusinessStatusDTO> BusinessStatus { get; set; }
			public PostalAddressDTO PostalAddress { get; set; }
			public DisplayPointDTO DisplayPoint { get; set; }
			public string HomePage { get; set; }
			public PhoneNumberDTO PhoneNumber { get; set; }
			public List<PhoneNumberDTO> AlternativePhoneNumber { get; set; }
			public List<CategoryDTO> Categories { get; set; }
			public List<OpeningHoursDTO> OpeningHours { get; set; }
			public string Chain { get; set; }
			public bool NeedUpdate { get; set; }
			public List<ContentDTO> Content { get; set; }
			public string Locale { get; set; }
		}

		public class ContentDTO
		{
			public string Content { get; set; }
			public string Locale { get; set; }
		}

		public class DisplayPointDTO
		{

			public CoordinatesDTO Coordinates { get; set; }
			public string Source { get; set; }
		}
		public class CoordinatesDTO
		{
			public string Latitude { get; set; }
			public string Longitude { get; set; }
		}
		public class BusinessStatusDTO
		{
			public string Status { get; set; }
			public string CorrectSiteCode { get; set; }
			public DateTime? OpenDate { get; set; }
			public DateTime? CloseDate { get; set; }
			public DateTime? ReOpenDate { get; set; }
			public bool CanReopen { get; set; }
		}
		public class PhoneNumberDTO
		{
			public string PhoneNumber { get; set; }
			public string Type { get; set; }
			public string Intent { get; set; }
		}
		public class PostalAddressDTO
		{
			public string FullAddress { get; set; }
			public List<StructuredAddressDTO> StructuredAddress { get; set; }
		}
		public class StructuredAddressDTO
		{
			public List<string> AddressLines { get; set; }
			public string Floor { get; set; }
			public string Neighborhood { get; set; }
			public string Locality { get; set; }
			public string StateProvince { get; set; }
			public string PostalCode { get; set; }
			public string CountryCode { get; set; }
		}
		public class StoreNameDTO
		{
			public string Name { get; set; }
			public string Locale { get; set; }
			public string Pronunciation { get; set; }

		}
		public class CategoryDTO
		{
			public string Category { get; set; }
			public string CategoryId { get; set; }
			public List<string> ServiceType { get; set; }

		}
		public class OpeningHoursDTO
		{
			public string Day { get; set; }
			public string OpenTime  { get; set; }
			public string CloseTime { get; set; }

		}

		public class BusinessDTO
		{
			public string locationId { get; set; }
			public string locationSource { get; set; }
			public string locationDetails { get; set; }
		}

        public class CSVFileDataDTO
        {
            public string LocationCode { get; set; }
            public string LocationName { get; set; }

        }

        public class LocationLogDetails
        {
            public string LocationCode { get; set; }
            public string LocationName { get; set; }
            public string LocationStatus { get; set; }
            public string LocationMessage { get; set; }
            public string AppleStatus { get; set; }
            public string AppleMessage { get; set; }
            public string GMBStatus { get; set; }
            public string GMBMessage { get; set; }
        }
    }
}
	