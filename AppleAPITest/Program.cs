﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Web.Script.Serialization;
using Amazon;
using Amazon.S3;
using Amazon.S3.IO;
using Amazon.S3.Model;
using Amazon.S3.Transfer;
using AppleAPITest.Helper;
using CsvHelper;
using CsvHelper.Configuration;
using Google.Apis.Auth.OAuth2;
using Google.Apis.Auth.OAuth2.Flows;
using Google.Apis.Indexing.v3;
using Google.Apis.Indexing.v3.Data;
using Google.Apis.MyBusinessBusinessInformation.v1.Data;
using Google.Apis.Services;
using iText.Kernel.Pdf;
using iText.Kernel.Utils;
using iText.Layout;

namespace AppleAPITest
{
	public static class Program
	{
		public static async System.Threading.Tasks.Task CreateBusinessAsync()
		{
			var accessToken = await AppleHelper.GetAppleAccessTokenAsync();

			//Get Business Details
			//var appleBusinessDetails = await AppleBusinessHelper.GetAppleBusinessDetailsAsync(AppleBusinessHelper.GetAppleBusiness1(AppleBusinessHelper.GetCreateAppleBusiness1()), accessToken);

			//var a = 1;
			//var business1 = AppleBusinessHelper.GetAppleBusiness1();

			//Create Business
			//var returnValue1 = await AppleBusinessHelper.CreateAppleBusinessAsync(AppleBusinessHelper.GetAppleBusiness1(), accessToken);
			//var returnValue2 = await AppleBusinessHelper.CreateAppleBusinessAsync(AppleBusinessHelper.GetAppleBusiness2(), accessToken);
			//var returnValue3 = await AppleBusinessHelper.CreateAppleBusinessAsync(AppleBusinessHelper.GetAppleBusiness3(), accessToken);
			//var returnValue4 = await AppleBusinessHelper.CreateAppleBusinessAsync(AppleBusinessHelper.GetAppleBusiness4(), accessToken);
			//var returnValue5 = await AppleBusinessHelper.CreateAppleBusinessAsync(AppleBusinessHelper.GetAppleBusiness5(), accessToken);

			//Update Categories
			//var appleObject1 = AppleBusinessHelper.GetAppleBusiness1(AppleBusinessHelper.GetCreateAppleBusiness1());
			//var categories1 = new List<string>() { "shopping.media", "professional.advertising" };
			//appleObject1.businessDetails.categories = categories1;
			//var returnValue1 = await AppleBusinessHelper.UpdateAppleBusinessAsync(appleObject1, accessToken);

			//var appleObject2 = AppleBusinessHelper.GetAppleBusiness2(AppleBusinessHelper.GetCreateAppleBusiness2());
			//var categories2 = new List<string>() { "restaurants.hotdogs" };
			//appleObject2.businessDetails.categories = categories2;
			//var returnValue2 = await AppleBusinessHelper.UpdateAppleBusinessAsync(appleObject2, accessToken);

			//var appleObject3 = AppleBusinessHelper.GetAppleBusiness3(AppleBusinessHelper.GetCreateAppleBusiness3());
			//var categories3 = new List<string>() { "food.gourmet.pastashops" };
			//appleObject3.businessDetails.categories = categories3;
			//var returnValue3 = await AppleBusinessHelper.UpdateAppleBusinessAsync(appleObject3, accessToken);

			//Delete Business
			//var appleObject5 = AppleBusinessHelper.GetAppleBusiness5(AppleBusinessHelper.GetCreateAppleBusiness5());
			//var returnValue5 = await AppleBusinessHelper.DeleteAppleBusinessAsync(appleObject5, accessToken);

			//var appleObject6 = AppleBusinessHelper.GetAppleBusiness6(AppleBusinessHelper.GetCreateAppleBusiness6());
			//var returnValue6 = await AppleBusinessHelper.DeleteAppleBusinessAsync(appleObject6, accessToken);

			//var appleObject7 = AppleBusinessHelper.GetAppleBusiness7(AppleBusinessHelper.GetCreateAppleBusiness7());
			//var returnValue7 = await AppleBusinessHelper.DeleteAppleBusinessAsync(appleObject7, accessToken);

			//List


			//Upload Image
			//var appleObject1 = AppleBusinessHelper.GetAppleMedia1();var returnValue1 = await AppleBusinessHelper.UploadImage(appleObject1, accessToken);

			//var appleObject2 = AppleBusinessHelper.GetAppleMedia2(); var returnValue2 = await AppleBusinessHelper.UploadImage(appleObject2, accessToken);
			//var appleObject3 = AppleBusinessHelper.GetAppleMedia3(); var returnValue3 = await AppleBusinessHelper.UploadImage(appleObject3, accessToken);
			//var appleObject4 = AppleBusinessHelper.GetAppleMedia4(); var returnValue4 = await AppleBusinessHelper.UploadImage(appleObject4, accessToken);
			//var appleObject5 = AppleBusinessHelper.GetAppleMedia5(); var returnValue5 = await AppleBusinessHelper.UploadImage(appleObject5, accessToken);
			//var appleObject6 = AppleBusinessHelper.GetAppleMedia6(); var returnValue6 = await AppleBusinessHelper.UploadImage(appleObject6, accessToken);
			//var appleObject7 = AppleBusinessHelper.GetAppleMedia7(); var returnValue7 = await AppleBusinessHelper.UploadImage(appleObject7, accessToken);
			//var appleObject8 = AppleBusinessHelper.GetAppleMedia8(); var returnValue8 = await AppleBusinessHelper.UploadImage(appleObject8, accessToken);
			//var appleObject9 = AppleBusinessHelper.GetAppleMedia9(); var returnValue9 = await AppleBusinessHelper.UploadImage(appleObject9, accessToken);
			//var appleObject10 = AppleBusinessHelper.GetAppleMedia10(); var returnValue10 = await AppleBusinessHelper.UploadImage(appleObject10, accessToken);
			//var appleObject11 = AppleBusinessHelper.GetAppleMedia11(); var returnValue11 = await AppleBusinessHelper.UploadImage(appleObject11, accessToken);
			//var appleObject12 = AppleBusinessHelper.GetAppleMedia12(); var returnValue12 = await AppleBusinessHelper.UploadImage(appleObject12, accessToken);
			//var appleObject13 = AppleBusinessHelper.GetAppleMedia13(); var returnValue13 = await AppleBusinessHelper.UploadImage(appleObject13, accessToken);
			//var appleObject14 = AppleBusinessHelper.GetAppleMedia14(); var returnValue14 = await AppleBusinessHelper.UploadImage(appleObject14, accessToken);
			//var appleObject15 = AppleBusinessHelper.GetAppleMedia15(); var returnValue15 = await AppleBusinessHelper.UploadImage(appleObject15, accessToken);
			//var appleObject16 = AppleBusinessHelper.GetAppleMedia16(); var returnValue16 = await AppleBusinessHelper.UploadImage(appleObject16, accessToken);

			//var metadata1 = await AppleBusinessHelper.GetMediaMetadataAsync("7893a1dd-a151-4cd9-a64c-9dbeda3ad1c4", accessToken); Console.Write(metadata1);
			//var metadata2 = await AppleBusinessHelper.GetMediaMetadataAsync("5143e64e-a699-4176-9318-8e8b9108aa5f", accessToken); Console.Write(metadata2);
			//var metadata3 = await AppleBusinessHelper.GetMediaMetadataAsync("25e04557-0eeb-4aaa-85dc-7b27e6124b52", accessToken); Console.Write(metadata3);
			//var metadata4 = await AppleBusinessHelper.GetMediaMetadataAsync("315f5c24-b9a9-4558-ae97-cd11adf41d82", accessToken); Console.Write(metadata4);
			//var metadata5 = await AppleBusinessHelper.GetMediaMetadataAsync("9ba52e0a-e3cb-4b28-8dcb-03c01ec7aa4e", accessToken); Console.Write(metadata5);
			//var metadata6 = await AppleBusinessHelper.GetMediaMetadataAsync("9263befb-a550-45b0-a893-5bf22b5f3286", accessToken); Console.Write(metadata6);
			//var metadata7 = await AppleBusinessHelper.GetMediaMetadataAsync("0ba8cf58-87ad-4813-848d-a5862505df11", accessToken); Console.Write(metadata7);
			//var metadata8 = await AppleBusinessHelper.GetMediaMetadataAsync("1e471cb4-48d6-4c14-954c-d90678fcfb6f", accessToken); Console.Write(metadata8);
			//var metadata9 = await AppleBusinessHelper.GetMediaMetadataAsync("2825c8e7-13dc-4644-9839-3c5a2712f723", accessToken); Console.Write(metadata9);
			//var metadata10 = await AppleBusinessHelper.GetMediaMetadataAsync("f640d504-8cc8-47db-aaff-4eb16912b784", accessToken); Console.Write(metadata10);
			//var metadata11 = await AppleBusinessHelper.GetMediaMetadataAsync("775efd3a-5541-45ee-85e9-9ba3b24b933c", accessToken); Console.Write(metadata11);
			//var metadata12 = await AppleBusinessHelper.GetMediaMetadataAsync("65d1008b-43c5-4834-8cc2-6337391b4bc8", accessToken); Console.Write(metadata12);
			//var metadata13 = await AppleBusinessHelper.GetMediaMetadataAsync("bcc1f9d3-7762-4a43-b8b5-3c44f48a340c", accessToken); Console.Write(metadata13);
			//var metadata14 = await AppleBusinessHelper.GetMediaMetadataAsync("04da717c-47d7-4f5d-9acc-9b4cb439ab6e", accessToken); Console.Write(metadata14);
			//var metadata15 = await AppleBusinessHelper.GetMediaMetadataAsync("8345c951-f77a-44f7-9c67-4148bfdeffd6", accessToken); Console.Write(metadata15);



			//var appleObject2 = AppleBusinessHelper.GetAppleBusiness2(AppleBusinessHelper.GetCreateAppleBusiness2());
			//var categories2 = new List<string>() { "restaurants.hotdogs" };
			//appleObject2.businessDetails.categories = categories2;
			//var returnValue2 = await AppleBusinessHelper.UpdateAppleBusinessAsync(appleObject2, accessToken);

			//var appleObject3 = AppleBusinessHelper.GetAppleBusiness3(AppleBusinessHelper.GetCreateAppleBusiness3());
			//var categories3 = new List<string>() { "food.gourmet.pastashops" };
			//appleObject3.businessDetails.categories = categories3;
			//var returnValue3 = await AppleBusinessHelper.UpdateAppleBusinessAsync(appleObject3, accessToken);




			//var myData = new
			//{
			//	FirstName = "John",
			//	LastName = "Doe",
			//	Age = 30,
			//	IsStudent = false,
			//	Address = new
			//	{
			//		Street = "123 Main St",
			//		City = "Anytown",
			//		ZipCode = "12345"
			//	}
			//};

			//// Convert the C# object to a JSON string
			//string jsonString = JsonConvert.SerializeObject(myData, Formatting.Indented);

			//// Print or use the JSON string as needed
			//Console.WriteLine(jsonString);


			//var business01 = @"{
			//						"businessDetails": {
			//						"partnersBusinessId": "RD001",
			//						"partnersBusinessVersion": "RD001V01",
			//						"countryCodes": [
			//							"AU"
			//						],
			//						"displayNames": [
			//							{
			//								"name": "Resolution Digital - Australia",
			//								"locale": "en",
			//								"primary": true
			//							}
			//						],
			//						"categories": [
			//							"shopping.media"
			//						],
			//						"urls": [
			//							{
			//								"url": "http://resolutiondigital.com.au/",
			//								"type": "HOMEPAGE"
			//							}
			//						]
			//					}
			//				}";
		}
		static void Main()
		{


			//string serviceAccountKeyPath = "C:\\Users\\SuvashShrestha(OMG)\\Downloads\\theinternetmarketingtools1-5a9dd475cec1.json";

			//GoogleCredential credential;
			//using (var stream = new FileStream(serviceAccountKeyPath, FileMode.Open, FileAccess.Read)) { credential = GoogleCredential.FromStream(stream).CreateScoped(IndexingService.Scope.Indexing); }

			//var service = new IndexingService(new BaseClientService.Initializer() { HttpClientInitializer = credential });

			//string urlToIndex = "www.omgtransact.com.au/about-us/"; //"https://www.omgtransact.com.au/";

			//var urlAdd = new UrlNotification { Url = urlToIndex, Type = "URL_UPDATED" };

			//try
			//{
			//	var request = service.UrlNotifications.Publish(urlAdd);
			//	request.Execute();
			//	Console.WriteLine($"Indexing request for {urlToIndex} sent successfully.");
			//}
			//catch (Exception ex)
			//{
			//	Console.WriteLine($"Error sending indexing request: {ex.Message}");
			//	return;
			//}

			//try
			//{
			//	var statusRequest = service.UrlNotifications.GetMetadata();
			//	statusRequest.Url = urlToIndex;
			//	var status = statusRequest.Execute();

			//	Console.WriteLine($"Indexing status for {urlToIndex}: {status.LatestUpdate}");
			//}
			//catch (Exception ex)
			//{
			//	Console.WriteLine($"Error getting indexing status: {ex.Message}");
			//}

			//var urlRemove = new UrlNotification { Url = urlToIndex, Type = "URL_DELETED" };

			//try
			//{
			//	var request = service.UrlNotifications.Publish(urlRemove);
			//	request.Execute();
			//	Console.WriteLine($"Removal request for {urlToIndex} sent successfully.");
			//}
			//catch (Exception ex)
			//{
			//	Console.WriteLine($"Error sending removal request: {ex.Message}");
			//}

			//try
			//{
			//	var statusRequest = service.UrlNotifications.GetMetadata();
			//	statusRequest.Url = urlToIndex;
			//	var status = statusRequest.Execute();

			//	Console.WriteLine($"Indexing status for {urlToIndex}: {status.LatestUpdate}");
			//}
			//catch (Exception ex)
			//{
			//	Console.WriteLine($"Error getting indexing status: {ex.Message}");
			//}


			//try
			//{
			//	var statusRequest = service.UrlNotifications.GetMetadata();
			//	statusRequest.Url = "https://www.omgtransact.com.au/";
			//	var status = statusRequest.Execute();

			//	Console.WriteLine($"Indexing status for {urlToIndex}: {status.LatestUpdate}");
			//}
			//catch (Exception ex)
			//{
			//	Console.WriteLine($"Error getting indexing status: {ex.Message}");
			//}

			//return;

			var locationDetails = new List<DataModel.LocationLogDetails>();

			var clientId = "917729250995-oerfrs4tqlih62d5ao4s66d6vvndfpi2.apps.googleusercontent.com";
			var clientSecret = "aTKoIkToUmgCx1ox3EWtet_e";

			//Coles
			//var accessToken = "ya29.a0AfB_byBTY0c2eGoeaHwsaBwxFsk6meqHojAUwTg8ocjWKEI915C3BuHfjxFyPVqDL6qJgaoShmNfvhwoiBcWBuegjHHIohEN05vSenrdYP9NqsrL8T6TeFIw5GTqCVWo-POEfMuu_bgubmh_tYWlOtQLcxM8BVHSQTkXaCgYKAWwSARASFQHGX2MiB3fnNZbL-s69-X0iotN0BA0171";
			//var refreshToken = "1//0gqIgpazSalxvCgYIARAAGBASNwF-L9IrQxmHUhxp2P2DEBcsRCcE0IoZRGGe5yw86LwVDNERQEN-B6LLWCB_4Rbe-7Ojmugy0M8";

			//maurice
			//var accessToken = "ya29.a0AfB_byDjj4g86yoQJjid0t9gXhTyLTqqPjia8KuJaceCmuH4anEoNtlWbIcNsLx2OCXiwIo55fpS_3LTAazWm6N-GINRoXr6YpExBs14N7BuCXk1f9-FlB6hM5N5mmbaLUa0uq2BLMLaKgLIg1esgUlZCbf1cX_Je0zEaCgYKAY8SARMSFQGOcNnCasaw8QfOxhbJh9ZxrxBwTQ0171";
			//var refreshToken = "1//0gZxG8tXvD6Z_CgYIARAAGBASNwF-L9IrxrXfPzmUqY-KTeGAcAvIQvzhnB3MDD2SOvajdFKVGgIgf3c3n4Ao6zqLmVfzzbjDttw";


			//McDonald
			//var accessToken = "ya29.a0AfB_byA_ampS8Tc1QDM2Vl7vKN8rNR6jCuyktaXPuBcbDShEmmwGsmlOPsdXyklJ_nCwZJL0NOfLcgvbCKxLjFms48Lb2zTmbaXdfhbvsh1Ugmr-hMoza5BFmy8QC0iXHCUj814b7081cZ1j5BRLPhAKu9uLJ8n2cbh_aCgYKATASARASFQHGX2MixYQ-NuoxYYBaYdzqACet-A0171";
			//var refreshToken = "1//0gCeBCEeRMVewCgYIARAAGBASNwF-L9IrHb92LaNAcQ1tFCvYJhJjTqOkVVj9nBb_acecxyQrgx6CRHMyrDQBksUV-wjbN3Rubb8";

			//resolution
			//var accessToken = "ya29.a0AfB_byDRHwtxsvn6Nzt5zfbRJ92wMuE2PaQWgqUBtHhY7d29WSfSAyXmk_oHSzTbKWL2YSWezOnUvd5sT9l5BCa768YFznzDjCjXMgKrH_1D_o7zIqMiDOuyHW08HJSHG08sY41GC6m9D0Ny-L4GH-0PZHrzcVIPalV9_waCgYKAeISARMSFQHsvYls88M5wa2W91v691x-HlfB1w0173";
			//var refreshToken = "1//0gtJ20VaWvPjmCgYIARAAGBASNwF-L9IrSPGEMtBU9NFA71PYIzdawvriJ96wxFrgF4anaA-jWi1p3GhuqWAgXLP4ttrNx7bCkBo";

			//BUPA
			//var accessToken = "ya29.a0AfB_byANFYM4Oh4k_E__YQ1ZWb2glBckUvcGCBSuFh1682LqKyvQaH3aE7FierZb6iVKEHEwwkaE6HuDRtCdLcwQBRaeoQ455MOuF90IAGuokh45OuaonyM_D5hEut3i5z2JWhfjtJtssfuLJSM4IN8v01GAWgnv5at2aCgYKAZ0SARISFQGOcNnCu7ZIb9lTn1OhrkHnjtFcCA0171";
			//var refreshToken = "1//0ei3ek6G7W53ICgYIARAAGA4SNwF-L9Iriqmfp3Y2NxwfEQ5tz4_j4CIvJDAGJCerroubG1DXIR6IdMc5jl_Q7GYw8cVQyIXGCiE";

			////McDonald
			var accessToken = "ya29.a0AfB_byA_ampS8Tc1QDM2Vl7vKN8rNR6jCuyktaXPuBcbDShEmmwGsmlOPsdXyklJ_nCwZJL0NOfLcgvbCKxLjFms48Lb2zTmbaXdfhbvsh1Ugmr-hMoza5BFmy8QC0iXHCUj814b7081cZ1j5BRLPhAKu9uLJ8n2cbh_aCgYKATASARASFQHGX2MixYQ-NuoxYYBaYdzqACet-A0171";
			var refreshToken = "1//0gCeBCEeRMVewCgYIARAAGBASNwF-L9IrHb92LaNAcQ1tFCvYJhJjTqOkVVj9nBb_acecxyQrgx6CRHMyrDQBksUV-wjbN3Rubb8";

			//ooles
			//var accessToken = "ya29.a0AfB_byBTY0c2eGoeaHwsaBwxFsk6meqHojAUwTg8ocjWKEI915C3BuHfjxFyPVqDL6qJgaoShmNfvhwoiBcWBuegjHHIohEN05vSenrdYP9NqsrL8T6TeFIw5GTqCVWo-POEfMuu_bgubmh_tYWlOtQLcxM8BVHSQTkXaCgYKAWwSARASFQHGX2MiB3fnNZbL-s69-X0iotN0BA0171";
			//var refreshToken = "1//0gqIgpazSalxvCgYIARAAGBASNwF-L9IrQxmHUhxp2P2DEBcsRCcE0IoZRGGe5yw86LwVDNERQEN-B6LLWCB_4Rbe-7Ojmugy0M8";


			var myBusinessAccountService = GMBHelper.GetGMBAccount(clientId, clientSecret, accessToken, refreshToken);
			var myBusinessService = GMBHelper.GetGMBBusiness(clientId, clientSecret, accessToken, refreshToken);
			var myBusinessVerificationService = GMBHelper.GetGMBVerifications(clientId, clientSecret, accessToken, refreshToken);
			//v/*ar categoryList = new List<string>() { "restaurants.burgers" };*/
			//var businessObject = GMBHelper.GetAllLocation(myBusinessAccountService, myBusinessService, "7Eleven", categoryList, "7Eleven", locationDetails);
			//var businessId = "1530079702340337664";
			//var website = "https://mcdonalds.co.nz/";

			//GMBHelper.GetAllLocationVerification(myBusinessAccountService, myBusinessService, myBusinessVerificationService);
			///
			GMBHelper.GetAllLocationAppleObject(myBusinessAccountService, myBusinessService);
			//GMBHelper.DeleteAppleObject();
			//var list = myBusinessService.Locations.Get("locations/7316363869875474241");
			//var newBusiness = new Location();

			//newBusiness.Title = "New Business";
			//newBusiness.Profile =  new Profile() {Description = " Test the business gere"};
			//newBusiness.LanguageCode = "en";

			//var c = new Categories();
			//c.PrimaryCategory = new Category() {Name = "gcid:medical_center"};
			//newBusiness.Categories = c;

			//var sf = new PostalAddress();
			//sf.AddressLines = new List<string>() {"Ganga Hall Marga"};
			//sf.RegionCode = "NP";
			//sf.Locality = "Kathmandu";
			//sf.PostalCode = "44600";
			//newBusiness.StorefrontAddress = sf;

			//newBusiness.WebsiteUri= "https://restroa.com.np/";

			//var newB= myBusinessService.Accounts.Locations.Create(newBusiness, "accounts/117261759520178272836");
			//	newB.Execute();
			//	list.ReadMask = "regularHours";
			//	var postResult = list.Execute();

			//	var a1 = myBusinessService.Attributes.List();//   ("locations/7316363869875474241");
			//	var abc = a1.Execute();

			//	if (abc != null)
			//	{
			//       }




			//######### CREATE BUSINESS
			//CreateBusinessAsync().GetAwaiter().GetResult();

			//return;



			//Connect Google My Business




			//string bucketName = "omg-skynet";
			//string awsAccessKey = "AKIA56TDZX6D56QTBXV3";
			//string awsSecretKey = "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";

			//IAmazonS3 client = new AmazonS3Client(awsAccessKey, awsSecretKey, RegionEndpoint.APSoutheast2);

			////using (AmazonS3Client clientConnection = new AmazonS3Client(basicCredentials, configurationClient))
			////{

			//    S3FileInfo file1 = new S3FileInfo(client, bucketName, "1349/Internal Projects 07 May.docx");
			//   var l = file1.Exists;//if the file exists return true, in other case false
			////}

			////Listing content of a folder
			//// The following code would list the contents of sub - folder,
			//ListObjectsRequest request = new ListObjectsRequest
			//{
			//    BucketName = bucketName,
			//    //Prefix = "my-folder/sub-folder/"
			//};
			//var a = 1;
			//ListObjectsResponse response = client.ListObjects(request);
			//foreach (S3Object obj in response.S3Objects)
			//{
			//    Console.WriteLine(obj.Key);
			//}

			//PutObjectRequest request1 = new PutObjectRequest()
			//{
			//    BucketName = bucketName,
			//    Key = "1349/" // <-- in S3 key represents a path  
			//};
			//PutObjectResponse response1 = client.PutObject(request1);

			//FileInfo file = new FileInfo(@"C:\\_desktop\\_weekly_project\\Internal Projects 07 May.docx");
			//string path = "1349/Internal Projects 07 May.docx";
			//PutObjectRequest request2 = new PutObjectRequest()
			//{
			//    BucketName = bucketName,
			//    InputStream = file.OpenRead(),
			//    //Prefix = "1349/",
			//    Key = path  // <-- in S3 key represents a path  
			//};
			//PutObjectResponse response2 = client.PutObject(request2);

			////string filePath = "1349/Internal Projects 07 May.docx";
			////var deleteFileRequest = new DeleteObjectRequest
			////{
			////    BucketName = bucketName,
			////    Key = filePath
			////};
			////DeleteObjectResponse fileDeleteResponse = client.DeleteObject(deleteFileRequest);

			//GetObjectRequest getRequest = new GetObjectRequest()
			//{
			//    BucketName = bucketName,
			//    Key = path
			//};
			//GetObjectResponse getResponse = client.GetObject(getRequest);
			//getResponse.WriteResponseStreamToFile(@"C:\\_desktop\\_weekly_project\\Internal Projects 07 May 1.docx");
			////Creating a folder

			////    Here we are going to create a folder called sub - folder inside my-folder
			//string folderPath = "my-folder/sub-folder/";



			//PutObjectResponse response = client.PutObject(request);


			// string bucketName; string subDirectoryInBucket; string fileNameInS3;


			// IAmazonS3 client = new AmazonS3Client(RegionEndpoint.APSouth1);
			// TransferUtility utility = new TransferUtility(client);
			// TransferUtilityUploadRequest request = new TransferUtilityUploadRequest();

			// //if (subDirectoryInBucket == "" || subDirectoryInBucket == null)
			//// {
			//     request.BucketName = bucketName; //no subdirectory just bucket name  
			//// }
			//// else
			// //{   
			//  //   request.BucketName = bucketName + @"/" + subDirectoryInBucket;
			//// }
			// request.Key = fileNameInS3; //file name up in S3  
			// request.
			// //request.InputStream = localFilePath;
			// utility.Upload(request); //commensing the transfer  

			////return true; //indicate that the file was sent  
			//var locationDetails = new List<DataModel.LocationLogDetails>();
			//var clientId = "917729250995-oerfrs4tqlih62d5ao4s66d6vvndfpi2.apps.googleusercontent.com";
			//var clientSecret = "aTKoIkToUmgCx1ox3EWtet_e";

			////7Eleven
			//var accessToken = "ya29.a0AfB_byBTY0c2eGoeaHwsaBwxFsk6meqHojAUwTg8ocjWKEI915C3BuHfjxFyPVqDL6qJgaoShmNfvhwoiBcWBuegjHHIohEN05vSenrdYP9NqsrL8T6TeFIw5GTqCVWo-POEfMuu_bgubmh_tYWlOtQLcxM8BVHSQTkXaCgYKAWwSARASFQHGX2MiB3fnNZbL-s69-X0iotN0BA0171";
			//var refreshToken = "1//0gqIgpazSalxvCgYIARAAGBASNwF-L9IrQxmHUhxp2P2DEBcsRCcE0IoZRGGe5yw86LwVDNERQEN-B6LLWCB_4Rbe-7Ojmugy0M8";

			//// var clientId = "917729250995-oerfrs4tqlih62d5ao4s66d6vvndfpi2.apps.googleusercontent.com";
			//// var clientSecret = "aTKoIkToUmgCx1ox3EWtet_e";
			////var accessToken = "ya29.a0ARrdaM9eMYMVpXxOcFH1ozlcunQK7CYCd5wBP6Fzi8nEsSZODR-UINvwSq7Fy_78SLNTzhulUe9De91Ihuosvx-ngi_s1e4HXYC58_uIFdyD4oBRrRD095hskWOtvoqhTOBc6FWlN4-IFoE5tfv5EIhOGCxB";
			////var refreshToken = "1//0gol_YDzUpLOWCgYIARAAGBASNwF-L9IrYdtV49K1RuRJRf15xayuRAlRDUPrXx3Iw-O57QaDYJ1NKIQIHddtWm_YUDK0wdH3MZU";


			//var myBusinessAccountService = GMBHelper.GetGMBAccount(clientId, clientSecret, accessToken, refreshToken);
			//var myBusinessService = GMBHelper.GetGMBBusiness(clientId, clientSecret, accessToken, refreshToken);
			//var categoryList = new List<string>() { "shopping.deptstores" };
			//var businessObject = GMBHelper.GetAllLocation(myBusinessAccountService, myBusinessService, "7Eleven", categoryList, "7Eleven", locationDetails);

			/////

			//var list = myBusinessService.Locations.Get("locations/7316363869875474241");
			//var newBusiness = new Location();

			//newBusiness.Title = "New Business";
			//newBusiness.Profile =  new Profile() {Description = " Test the business gere"};
			//newBusiness.LanguageCode = "en";

			//var c = new Categories();
			//c.PrimaryCategory = new Category() {Name = "gcid:medical_center"};
			//newBusiness.Categories = c;

			//var sf = new PostalAddress();
			//sf.AddressLines = new List<string>() {"Ganga Hall Marga"};
			//sf.RegionCode = "NP";
			//sf.Locality = "Kathmandu";
			//sf.PostalCode = "44600";
			//newBusiness.StorefrontAddress = sf;

			//newBusiness.WebsiteUri= "https://restroa.com.np/";

			//var newB= myBusinessService.Accounts.Locations.Create(newBusiness, "accounts/117261759520178272836");
			//	newB.Execute();
			//	list.ReadMask = "regularHours";
			//	var postResult = list.Execute();

			//	var a1 = myBusinessService.Attributes.List();//   ("locations/7316363869875474241");
			//	var abc = a1.Execute();

			//	if (abc != null)
			//	{
			//       }



			//         Logger.FileLogger("****** STARTING *******", Parameters.LogFile.LogFile.ToString());
			//         Logger.FileLogger("***********************", Parameters.LogFile.LogFile.ToString());

			////1.Get Data Mode for Dominos.
			//Logger.FileLogger("****** GETTING DOMINOS OBJECT *******", Parameters.LogFile.LogFile.ToString());
			//var businessObject = BusinessListingHelper.GetDominosBusinessListing(locationDetails, "DOMINOS");
			//	 Logger.FileLogger("****** GETTING DOMINOS OBJECT COMPLETED *******", Parameters.LogFile.LogFile.ToString());

			//if (businessObject.Any())
			//{
			//2. Implement override functionality
			//BusinessListingHelper.OverrideBusinessListing(businessObject, locationDetails);

			//3. Check if the business is updated
			//BusinessListingHelper.GetUpdatedBusiness(businessObject, locationDetails);

			//	Logger.FileLogger("****** GETTING APPLE JSON *******", Parameters.LogFile.LogFile.ToString());
			//	var json1= AppleHelper.GetAppleJson(businessObject, locationDetails);
			//	Logger.FileLogger("****** GETTING APPLE JSON COMPLETED *******", Parameters.LogFile.LogFile.ToString());

			//var retValue = BusinessListingHelper.UpdateGoogle(businessObject);
			//	CommonHelper.SendSummaryEmail(locationDetails);
			//}

			var a = 1;
		}












	}
}